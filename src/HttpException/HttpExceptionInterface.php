<?php

declare(strict_types=1);

namespace App\HttpException;

interface HttpExceptionInterface extends \Symfony\Component\HttpKernel\Exception\HttpExceptionInterface, \Throwable
{
}
