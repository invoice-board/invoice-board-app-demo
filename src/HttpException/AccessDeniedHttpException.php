<?php

declare(strict_types=1);

namespace App\HttpException;

class AccessDeniedHttpException extends \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException implements HttpExceptionInterface
{
}
