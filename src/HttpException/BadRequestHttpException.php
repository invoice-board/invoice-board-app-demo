<?php

declare(strict_types=1);

namespace App\HttpException;

class BadRequestHttpException extends \Symfony\Component\HttpKernel\Exception\BadRequestHttpException implements HttpExceptionInterface
{
}
