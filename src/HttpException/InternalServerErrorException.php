<?php

declare(strict_types=1);

namespace App\HttpException;

use Exception;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException as SymfonyHttpException;

class InternalServerErrorException extends SymfonyHttpException implements HttpExceptionInterface
{
    public function __construct(
        string $message = null,
        Exception $previous = null,
        int $code = 0,
        array $headers = []
    ) {
        parent::__construct(Response::HTTP_INTERNAL_SERVER_ERROR, $message, $previous, $headers, $code);
    }
}
