<?php

declare(strict_types=1);

namespace App\Helper;

class MoneyViewer
{
    public function asDollar(float $value): string
    {
        return '$' . $value;
    }
}
