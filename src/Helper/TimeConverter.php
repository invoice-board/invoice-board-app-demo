<?php

declare(strict_types=1);

namespace App\Helper;

class TimeConverter
{
    public function toDecimal(int $hours, int $minutes): float
    {
        return $hours + ($minutes / 60);
    }
}
