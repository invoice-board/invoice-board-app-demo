<?php

declare(strict_types=1);

namespace App\Helper;

class MoneyConverter
{
    public function centsToDollars(int $cents): float
    {
        $dollars = number_format($cents / 100, 2, '.', '');

        return (float) $dollars;
    }

    public function dollarsToCents(float $dollars): int
    {
        return (int) $dollars * 100;
    }
}
