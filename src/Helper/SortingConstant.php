<?php

declare(strict_types=1);

namespace App\Helper;

class SortingConstant
{
    public const DESC = 'desc';
    public const ACS = 'acs';
}
