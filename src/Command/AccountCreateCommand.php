<?php

declare(strict_types=1);

namespace App\Command;

use App\Model\Repository\AccountRepository;
use App\Service\Account\AccountService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class AccountCreateCommand extends Command
{
    private $accountService;
    private $accountRepository;

    public function __construct(AccountService $accountService, AccountRepository $accountRepository)
    {
        parent::__construct();

        $this->accountService = $accountService;
        $this->accountRepository = $accountRepository;
    }

    protected function configure(): void
    {
        $this
            ->setName('app:account:create')
            ->setDescription('Create new Account.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        // General Info
        $account = $this->accountService->create();
        $account->setName(
            (string) $this->ask($input, $output, 'Account name: ')
        );

        // Trello
        $trelloSettings = $account->getTrelloSettings();
        $trelloSettings->setApiKey(
            (string) $this->ask($input, $output, 'Trello API Key: ')
        );
        $trelloSettings->setApiSecret(
            (string) $this->ask($input, $output, 'Trello API Secret: ')
        );
        $trelloSettings->setBoardId(
            (string) $this->ask($input, $output, 'Trello Board ID: ')
        );

        // Harvest
        $harvestSettings = $account->getHarvestSettings();
        $harvestSettings->setApiToken(
            (string) $this->ask($input, $output, 'Harvest API Token: ')
        );
        $harvestSettings->setAccountId(
            (string) $this->ask($input, $output, 'Harvest Account ID: ')
        );

        // Telegram
        $telegramSettings = $account->getTelegramSettings();
        $telegramSettings->setBotToken(
            (string) $this->ask($input, $output, 'Telegram Bot Token: ')
        );
        $telegramSettings->setChatId(
            (string) $this->ask($input, $output, 'Telegram Chat ID: ')
        );

        // Save
        $dm = $this->accountRepository->getDocumentManager();
        $dm->persist($account);
        $dm->flush();

        $message = 'Account created with ID: ' . $account->getId();
        $output->writeln($message);

        return 0;
    }

    private function ask(InputInterface $input, OutputInterface $output, string $text)
    {
        $helper = $this->getHelper('question');
        $questionNormaliser = static function ($value) {
            // $value can be null here
            return $value ? trim($value) : '';
        };

        $question = new Question($text);
        $question->setNormalizer($questionNormaliser);

        return $helper->ask($input, $output, $question);
    }
}
