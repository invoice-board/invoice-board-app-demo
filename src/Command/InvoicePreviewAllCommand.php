<?php

declare(strict_types=1);

namespace App\Command;

use App\Helper\MoneyConverter;
use App\Helper\MoneyViewer;
use App\Model\Document\Account;
use App\Model\Document\Invoice;
use App\Model\Document\Task;
use App\Model\Repository\AccountRepository;
use App\Service\Invoice\InvoiceService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InvoicePreviewAllCommand extends Command
{
    private $invoiceService;
    private $accountRepository;
    private $moneyConverter;
    private $moneyViewer;

    public function __construct(
        InvoiceService $invoiceService,
        AccountRepository $accountRepository,
        MoneyConverter $moneyConverter,
        MoneyViewer $moneyViewer
    ) {
        parent::__construct();

        $this->invoiceService = $invoiceService;
        $this->accountRepository = $accountRepository;
        $this->moneyConverter = $moneyConverter;
        $this->moneyViewer = $moneyViewer;
    }

    public function configure(): void
    {
        $this->setName('app:invoice:preview-all');
        $this->setDescription('Show all Invoices for Account.');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $accounts = $this->accountRepository->findAll();

        /** @var Account $account */
        foreach ($accounts as $account) {
            $output->writeln('Account: ' . $account->getName());
            $output->writeln('');

            $invoices = $this->invoiceService->makeDrafts($account);

            /** @var Invoice $invoice */
            foreach ($invoices as $n => $invoice) {
                $this->invoiceService->savePreparation($invoice);

                $invoiceTotal = $this->moneyConverter->centsToDollars(
                    $invoice->getTotal()
                );

                $output->writeln('#' . $invoice->getName() . ' ' . $this->moneyViewer->asDollar($invoiceTotal));

                /** @var Task $task */
                foreach ($invoice->getTasks() as $task) {
                    $taskTotal = $this->moneyConverter->centsToDollars(
                        $task->getTotal()
                    );

                    $output->writeln($task->getName() . ' ' . $this->moneyViewer->asDollar($taskTotal));
                }

                $output->writeln('');
            }
        }

        $output->writeln('Done.');

        return 0;
    }
}
