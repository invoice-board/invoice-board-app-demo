<?php

declare(strict_types=1);

namespace App\Command;

use App\Model\Document\Account;
use App\Model\Document\Invoice;
use App\Model\Repository\AccountRepository;
use App\Model\Repository\InvoiceRepository;
use App\Service\Invoice\InvoiceService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InvoiceReSaveAllCommand extends Command
{
    private $invoiceService;
    private $accountRepository;
    private $invoiceRepository;

    public function __construct(
        InvoiceService $invoiceService,
        AccountRepository $accountRepository,
        InvoiceRepository $invoiceRepository
    ) {
        parent::__construct();

        $this->invoiceService = $invoiceService;
        $this->accountRepository = $accountRepository;
        $this->invoiceRepository = $invoiceRepository;
    }

    public function configure(): void
    {
        $this->setName('app:invoice:re-save-all');
        $this->setDescription('Re-save all Invoices in all Accounts.');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $accounts = $this->accountRepository->findAll();

        /** @var Account $account */
        foreach ($accounts as $account) {
            $invoices = $this->invoiceRepository->findAllAccount($account);

            /** @var Invoice $invoice */
            foreach ($invoices as $invoice) {
                if ($this->invoiceService->save($invoice)) {
                    $output->writeln($account->getName() . ' # Invoice is saved: ' . $invoice->getName());
                }
            }
        }

        $output->writeln('Done.');

        return 0;
    }
}
