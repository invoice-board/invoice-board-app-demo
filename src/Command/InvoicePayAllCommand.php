<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\Account\AccountService;
use App\Service\Invoice\InvoiceService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InvoicePayAllCommand extends Command
{
    private $invoiceService;
    private $accountService;

    public function __construct(
        InvoiceService $invoiceService,
        AccountService $accountService
    ) {
        parent::__construct();

        $this->invoiceService = $invoiceService;
        $this->accountService = $accountService;
    }

    public function configure(): void
    {
        $this->setName('app:invoice:pay-all');
        $this->setDescription('Pay all Unpaid Invoices.');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $account = $this->accountService->getCurrent();
        $payedInvoices = $this->invoiceService->payAll($account);

        $output->writeln('Find ' . $payedInvoices->count() . ' for pay.');

        foreach ($payedInvoices as $invoice) {
            $output->writeln('Invoice payed: ' . $invoice->getId());
        }

        $output->writeln('Done.');

        return 0;
    }
}
