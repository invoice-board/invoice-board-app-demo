<?php

declare(strict_types=1);

namespace App\Command;

use App\Model\Document\Account;
use App\Model\Document\Invoice;
use App\Model\Repository\AccountRepository;
use App\Service\Invoice\InvoiceService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InvoiceImportAllCommand extends Command
{
    private $invoiceService;
    private $accountRepository;

    public function __construct(
        InvoiceService $invoiceService,
        AccountRepository $accountRepository
    ) {
        parent::__construct();

        $this->invoiceService = $invoiceService;
        $this->accountRepository = $accountRepository;
    }

    public function configure(): void
    {
        $this->setName('app:invoice:import-all');
        $this->setDescription('Draft a new Invoice by Account.');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $accounts = $this->accountRepository->findAll();

        /** @var Account $account */
        foreach ($accounts as $account) {
            $invoices = $this->invoiceService->makeDrafts($account);

            /** @var Invoice $invoice */
            foreach ($invoices as $invoice) {
                if ($this->invoiceService->save($invoice)) {
                    $this->invoiceService->dispatchCreatedEvent($invoice);

                    $output->writeln($account->getName() . ' # Invoice created: ' . $invoice->getId());
                }

                // Need for proper sorting by created at
                sleep(1);
            }
        }

        $output->writeln('Done.');

        return 0;
    }
}
