<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\Invoice\InvoiceService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InvoiceDeleteCommand extends Command
{
    private const ARGUMENT_ID = 'id';

    private $invoiceService;

    public function __construct(InvoiceService $invoiceService)
    {
        parent::__construct('app:invoice:remove');

        $this->invoiceService = $invoiceService;
    }

    public function configure(): void
    {
        $this->setDescription('Remove a Invoice by Unique ID.');
        $this->addArgument(self::ARGUMENT_ID, InputArgument::REQUIRED, 'Invoice unique ID.');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $id = $input->getArgument(self::ARGUMENT_ID);

        $result = $this->invoiceService->remove($id);

        $output->writeln($result ? 'Removed.' : 'Fail.');

        return 0;
    }
}
