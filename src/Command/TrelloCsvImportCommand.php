<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\Account\AccountService;
use App\Service\Trello\TrelloClientFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class TrelloCsvImportCommand extends Command
{
    private const LIST_ID = 'list_id';
    private const PATH_TO_FILE = 'file';

    private $accountService;
    private $trelloClientFactory;

    public function __construct(AccountService $accountService, TrelloClientFactory $trelloClientFactory)
    {
        parent::__construct();

        $this->accountService = $accountService;
        $this->trelloClientFactory = $trelloClientFactory;
    }

    protected function configure(): void
    {
        $this
            ->setName('app:trello:csv:import')
            ->setDescription('Import task from CSV file in to Trello.')
            ->addArgument(self::LIST_ID, InputArgument::REQUIRED, 'Trello List ID.')
            ->addOption(self::PATH_TO_FILE, 'f', InputOption::VALUE_REQUIRED, 'Path to CSV file.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $account = $this->accountService->getCurrent();

        $trelloClient = $this->trelloClientFactory->create($account);

        $listId = $input->getArgument(self::LIST_ID);
        $filePath = $input->getOption(self::PATH_TO_FILE);

        if (false !== ($handler = fopen($filePath, 'r'))) {
            while (false !== ($data = fgetcsv($handler, 1000))) {
                $output->writeln("Name: {$data[0]}");
                $output->writeln("Description: {$data[1]}");
                $output->writeln("Time: {$data[3]}");
                $output->writeln('');

                $card = $trelloClient->cards()->create([
                    'idList' => $listId,
                    'name' => trim($data[0]),
                    'desc' => trim($data[1]),
                ]);

                $trelloClient->cards()->actions()->addComment($card['id'], ' @time ' . trim($data[3]));
            }
            fclose($handler);
        }

        $output->writeln('Completed!');

        return 0;
    }
}
