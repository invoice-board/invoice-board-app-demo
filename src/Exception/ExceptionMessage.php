<?php

declare(strict_types=1);

namespace App\Exception;

final class ExceptionMessage
{
    public const CURRENT_ACCOUNT_NOT_FOUND = 'Current account not found.';
    public const INVOICE_BY_ID_NOT_FOUND = 'Invoice ID=%s not found.';
    public const TASK_BY_ID_NOT_FOUND = 'Task ID=%s not found.';
    public const ACCESS_DENIED = 'You can not view this page.';
}
