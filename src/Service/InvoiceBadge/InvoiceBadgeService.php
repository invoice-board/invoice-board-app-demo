<?php

declare(strict_types=1);

namespace App\Service\InvoiceBadge;

use App\Event\InvoiceBadgeCreatedEvent;
use App\Exception\InvalidArgumentException;
use App\Model\Document\Invoice;
use App\Model\Document\InvoiceBadge;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class InvoiceBadgeService
{
    private $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    public function create(int $code): InvoiceBadge
    {
        $notification = new InvoiceBadge();
        $notification->setCode($code);

        return $notification;
    }

    public function append(Invoice $invoice, InvoiceBadge $badge): bool
    {
        $invoice->getBadges()->add($badge);

        return true;
    }

    public function clear(Invoice $invoice, int $groupCode = null): void
    {
        if (null !== $groupCode && false === \in_array($groupCode, InvoiceBadge::GROUPS, true)) {
            throw new InvalidArgumentException('Undefined group code: ' . $groupCode);
        }

        /** @var InvoiceBadge $badge */
        foreach ($invoice->getBadges() as $badge) {
            $notificationCode = $badge->getCode();

            if (null === $groupCode || ($groupCode <= $notificationCode && $groupCode + 100 > $notificationCode)) {
                $invoice->getBadges()->removeElement($badge);
            }
        }
    }

    public function dispatchCreatedEvent(Invoice $invoice, InvoiceBadge $badge): void
    {
        $this->eventDispatcher->dispatch(
            InvoiceBadgeCreatedEvent::NAME,
            new InvoiceBadgeCreatedEvent($invoice, $badge)
        );
    }
}
