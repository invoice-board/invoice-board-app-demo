<?php

declare(strict_types=1);

namespace App\Service\Task;

use App\Model\Document\Task;
use App\Model\Document\TimeEntry;
use App\Service\TimeEntry\TimeEntrySummaryService;

class TaskSummaryService
{
    private $timeEntrySummaryService;

    public function __construct(TimeEntrySummaryService $timeEntrySummaryService)
    {
        $this->timeEntrySummaryService = $timeEntrySummaryService;
    }

    public function calculateTotal(Task $task): int
    {
        $total = 0;

        /** @var TimeEntry $timeEntry */
        foreach ($task->getTimeEntries() as $timeEntry) {
            $timeEntryTotal = $this->timeEntrySummaryService->calculateTotal($timeEntry);
            $timeEntry->setTotal($timeEntryTotal);

            $total += $timeEntryTotal;
        }

        return $total;
    }

    public function calculateOriginalTotal(Task $task): int
    {
        $total = 0;

        /** @var TimeEntry $timeEntry */
        foreach ($task->getTimeEntries() as $timeEntry) {
            $timeEntryTotal = $this->timeEntrySummaryService->calculateOriginalTotal($timeEntry);

            $total += $timeEntryTotal;
        }

        return $total;
    }
}
