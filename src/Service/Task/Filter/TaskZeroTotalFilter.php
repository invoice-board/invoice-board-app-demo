<?php

declare(strict_types=1);

namespace App\Service\Task\Filter;

use App\Model\Collection\TaskCollection;
use App\Model\Document\Task;

class TaskZeroTotalFilter
{
    public function filter(TaskCollection $tasks): TaskCollection
    {
        return $tasks->filter(static function (Task $task) {
            return $task->getTotal() > 0;
        });
    }
}
