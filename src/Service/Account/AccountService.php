<?php

declare(strict_types=1);

namespace App\Service\Account;

use App\Exception\ExceptionMessage;
use App\Exception\NotFoundException;
use App\Model\Document\Account;
use App\Model\Repository\AccountRepository;

class AccountService
{
    private $accountRepository;

    public function __construct(AccountRepository $accountRepository)
    {
        $this->accountRepository = $accountRepository;
    }

    public function getCurrent(): Account
    {
        $list = $this->accountRepository->findAll();
        $account = reset($list);
        $account = false === $account ? null : $account;

        if (null === $account) {
            throw new NotFoundException(ExceptionMessage::CURRENT_ACCOUNT_NOT_FOUND);
        }

        return $account;
    }

    public function create(): Account
    {
        return new Account();
    }
}
