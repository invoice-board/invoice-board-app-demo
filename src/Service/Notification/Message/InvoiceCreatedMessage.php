<?php

declare(strict_types=1);

namespace App\Service\Notification\Message;

use App\Helper\MoneyConverter;
use App\Model\Document\Invoice;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class InvoiceCreatedMessage extends AbstractMessage
{
    private $moneyConverter;
    private $invoice;
    private $router;

    public function __construct(MoneyConverter $moneyConverter, RouterInterface $router, Invoice $invoice)
    {
        $this->moneyConverter = $moneyConverter;
        $this->invoice = $invoice;
        $this->router = $router;
    }

    public function getText(): string
    {
        $account = $this->invoice->getAccount();

        $invoiceTotal = $this->moneyConverter->centsToDollars(
            $this->invoice->getTotal()
        );
        $invoiceUrl = $this->router->generate(
            'invoice-update',
            ['id' => $this->invoice->getId()],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        return $this->getWriter()
            ->write('%s says:', [$account->getName()])->br()
            ->write('New invoice "%s" on $%s.', [$this->invoice->getName(), $invoiceTotal])->br()
            ->write('Please, process it now!')->br()
            ->addLink('Go to Invoice page', $invoiceUrl)->br()
            ->getText();
    }
}
