<?php

declare(strict_types=1);

namespace App\Service\Notification\Message;

use App\Helper\MoneyConverter;
use App\Model\Document\Invoice;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class InvoicePaidMessage extends AbstractMessage
{
    private $moneyConverter;
    private $invoice;
    private $router;

    public function __construct(MoneyConverter $moneyConverter, RouterInterface $router, Invoice $invoice)
    {
        $this->moneyConverter = $moneyConverter;
        $this->invoice = $invoice;
        $this->router = $router;
    }

    public function getText(): string
    {
        $account = $this->invoice->getAccount();
        $accountBalance = $this->moneyConverter->centsToDollars($account->getValue());
        $invoiceUrl = $this->router->generate(
            'invoice-view',
            ['id' => $this->invoice->getId()],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        return $this->getWriter()
            ->write('%s says:', [$account->getName()])->br()
            ->write('Invoice "%s" is fully paid.', [$this->invoice->getName()])->br()
            ->addLink('Go to Invoice page', $invoiceUrl)->br()
            ->write('Account balance: $%s', [$accountBalance])->br()
            ->getText();
    }
}
