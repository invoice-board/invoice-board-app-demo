<?php

declare(strict_types=1);

namespace App\Service\Notification\Message;

use App\Helper\MoneyConverter;
use App\Model\Document\Invoice;
use App\Service\Invoice\InvoiceService;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;

class NotEnoughMoneyMessage extends AbstractMessage
{
    private $moneyConverter;
    private $invoiceService;
    private $invoice;
    private $router;

    public function __construct(
        MoneyConverter $moneyConverter,
        InvoiceService $invoiceService,
        RouterInterface $router,
        Invoice $invoice
    ) {
        $this->moneyConverter = $moneyConverter;
        $this->invoiceService = $invoiceService;
        $this->invoice = $invoice;
        $this->router = $router;
    }

    public function getText(): string
    {
        $account = $this->invoice->getAccount();

        $invoiceLeftToPay = $this->moneyConverter->centsToDollars(
            $this->invoiceService->leftForPay($this->invoice)
        );
        $invoiceTotal = $this->moneyConverter->centsToDollars(
            $this->invoice->getTotal()
        );
        $refillUrl = $this->router->generate(
            'account-refill',
            [],
            UrlGeneratorInterface::ABSOLUTE_URL
        );

        return $this->getWriter()
            ->write('%s says:', [$account->getName()])->br()
            ->write(
                'There is not enough money in the account to pay an invoice "%s" on $%s.',
                [$this->invoice->getName(), $invoiceTotal]
            )->br()
            ->write(
                'Refill your account with at least $%s.',
                [$invoiceLeftToPay]
            )->br()
            ->addLink('Go to Refill page!', $refillUrl)->br()
            ->getText();
    }
}
