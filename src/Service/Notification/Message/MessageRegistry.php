<?php

declare(strict_types=1);

namespace App\Service\Notification\Message;

use App\Helper\MoneyConverter;
use App\Model\Document\Invoice;
use App\Service\Invoice\InvoiceService;
use Symfony\Component\Routing\RouterInterface;

class MessageRegistry
{
    private $moneyConverter;
    private $invoiceService;
    private $router;

    public function __construct(MoneyConverter $moneyConverter, InvoiceService $invoiceService, RouterInterface $router)
    {
        $this->moneyConverter = $moneyConverter;
        $this->invoiceService = $invoiceService;
        $this->router = $router;
    }

    public function getInvoiceCreated(Invoice $invoice): InvoiceCreatedMessage
    {
        return new InvoiceCreatedMessage($this->moneyConverter, $this->router, $invoice);
    }

    public function getInvoicePaid(Invoice $invoice): InvoicePaidMessage
    {
        return new InvoicePaidMessage($this->moneyConverter, $this->router, $invoice);
    }

    public function getNotEnoughMoneyMessage(Invoice $invoice): NotEnoughMoneyMessage
    {
        return new NotEnoughMoneyMessage($this->moneyConverter, $this->invoiceService, $this->router, $invoice);
    }

    public function getInvoicePublishedMessage(Invoice $invoice): InvoicePublishedMessage
    {
        return new InvoicePublishedMessage($this->moneyConverter, $this->router, $invoice);
    }
}
