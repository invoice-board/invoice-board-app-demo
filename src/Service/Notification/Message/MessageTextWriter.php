<?php

declare(strict_types=1);

namespace App\Service\Notification\Message;

class MessageTextWriter
{
    private $text = '';

    public function write(string $text, array $params = []): self
    {
        if (empty($params)) {
            $this->text .= $text;
        } else {
            $this->text .= sprintf($text, ...$params);
        }

        return $this;
    }

    public function br(): self
    {
        $this->text .= PHP_EOL;

        return $this;
    }

    public function addLink(string $text, string $url): self
    {
        return $this->write('[%s](%s)', [$text, $url]);
    }

    public function getText(): string
    {
        return $this->text;
    }
}
