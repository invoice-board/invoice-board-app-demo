<?php

declare(strict_types=1);

namespace App\Service\Notification\Message;

abstract class AbstractMessage
{
    abstract public function getText(): string;

    protected function getWriter(): MessageTextWriter
    {
        return new MessageTextWriter();
    }
}
