<?php

declare(strict_types=1);

namespace App\Service\Notification;

use App\Model\Document\Account;
use App\Service\Notification\Message\AbstractMessage;

class NotificationService
{
    public function send(Account $account, AbstractMessage $message): void
    {
        $telegramSettings = $account->getTelegramSettings();

        $query = http_build_query([
            'chat_id' => $telegramSettings->getChatId(),
            'text' => $message->getText(),
            'parse_mode' => 'Markdown',
        ]);
        $url = 'https://api.telegram.org/bot' . $telegramSettings->getBotToken() . '/sendMessage?' . $query;
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url,
        ]);
        $response = curl_exec($curl);
        $info = curl_getinfo($curl);
        curl_close($curl);
    }
}
