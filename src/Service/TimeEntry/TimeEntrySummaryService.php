<?php

declare(strict_types=1);

namespace App\Service\TimeEntry;

use App\Helper\MoneyConverter;
use App\Model\Document\TimeEntry;

class TimeEntrySummaryService
{
    private $moneyConverter;

    public function __construct(MoneyConverter $moneyConverter)
    {
        $this->moneyConverter = $moneyConverter;
    }

    public function calculateTotal(TimeEntry $timeEntry): int
    {
        return $this->moneyConverter->dollarsToCents(
            ceil($timeEntry->getTrackedHours() * $timeEntry->getHourlyRate())
        );
    }

    public function calculateOriginalTotal(TimeEntry $timeEntry): int
    {
        return $this->moneyConverter->dollarsToCents(
            ceil($timeEntry->getOriginalTrackedHours() * $timeEntry->getHourlyRate())
        );
    }
}
