<?php

declare(strict_types=1);

namespace App\Service\TimeEntry;

use App\Model\Collection\TimeEntryCollection;
use App\Model\Document\TimeEntry;

class TimeEntryService
{
    public function smartMerge(TimeEntryCollection $timeEntries, TimeEntry $newTimeEntry): TimeEntryCollection
    {
        $timeEntry = $timeEntries->findByDeveloperName(
            $newTimeEntry->getDeveloperName()
        );

        if (null === $timeEntry) {
            $timeEntries->add($newTimeEntry);
        } else {
            $trackedHours = $timeEntry->getTrackedHours() + $newTimeEntry->getTrackedHours();
            $timeEntry->setTrackedHours($trackedHours);
            $timeEntry->setOriginalTrackedHours($trackedHours);
        }

        return $timeEntries;
    }
}
