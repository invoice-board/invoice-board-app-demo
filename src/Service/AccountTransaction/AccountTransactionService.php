<?php

declare(strict_types=1);

namespace App\Service\AccountTransaction;

use App\Event\AccountTransactionCreatedEvent;
use App\Exception\InvalidArgumentException;
use App\Exception\ZeroBalanceException;
use App\Helper\MoneyConverter;
use App\Model\Document\Account;
use App\Model\Document\AccountTransaction;
use App\Model\DTO\AccountTransactionDTO;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class AccountTransactionService
{
    private $moneyConverter;
    private $eventDispatcher;

    public function __construct(MoneyConverter $moneyConverter, EventDispatcherInterface $eventDispatcher)
    {
        $this->moneyConverter = $moneyConverter;
        $this->eventDispatcher = $eventDispatcher;
    }

    public function createFromDTO(AccountTransactionDTO $transactionDTO): AccountTransaction
    {
        if ($transactionDTO->getValue() <= 0) {
            throw new InvalidArgumentException('The value must be greater than zero.');
        }

        $value = $this->moneyConverter->dollarsToCents(
            $transactionDTO->getValue()
        );

        return $this->create(
            $transactionDTO->getType(),
            $value,
            $transactionDTO->getDescription()
        );
    }

    public function create(string $type, int $value, string $description = ''): AccountTransaction
    {
        if ($value <= 0) {
            throw new InvalidArgumentException('The value must be greater than zero.');
        }

        $transaction = new AccountTransaction();
        $transaction->setType($type);
        $transaction->setDescription($description);
        $transaction->setValue($value);

        return $transaction;
    }

    public function apply(Account $account, AccountTransaction $transaction): AccountTransaction
    {
        $transaction->setAccount($account);

        $accountValue = $account->getValue();

        switch ($transaction->getType()) {
            case AccountTransaction::TYPE_DEBIT:
                $accountValue += $transaction->getValue();
                break;
            case AccountTransaction::TYPE_CREDIT:
                if ($accountValue <= 0) {
                    throw new ZeroBalanceException('Account balance in Zero.');
                }

                if ($transaction->getValue() > $accountValue) {
                    $transaction->setValue($accountValue);
                }

                $accountValue -= $transaction->getValue();
                break;
        }

        $account->setValue($accountValue);

        return $transaction;
    }

    public function dispatchCreatedEvent(AccountTransaction $transaction): void
    {
        $this->eventDispatcher->dispatch(
            AccountTransactionCreatedEvent::NAME,
            new AccountTransactionCreatedEvent($transaction)
        );
    }
}
