<?php

declare(strict_types=1);

namespace App\Service\InvoiceTransaction;

use App\Exception\InvalidArgumentException;
use App\Model\Document\AccountTransaction;
use App\Model\Document\Invoice;
use App\Model\Document\InvoiceTransaction;

class InvoiceTransactionService
{
    public function create(AccountTransaction $accountTransaction): InvoiceTransaction
    {
        if ($accountTransaction->getValue() <= 0) {
            throw new InvalidArgumentException('The value must be greater than zero.');
        }

        $transaction = new InvoiceTransaction();
        $transaction->setValue($accountTransaction->getValue());
        $transaction->setAccountTransaction($accountTransaction);

        return $transaction;
    }

    public function apply(Invoice $invoice, InvoiceTransaction $invoiceTransaction): void
    {
        $invoice->getTransactions()->add($invoiceTransaction);
    }
}
