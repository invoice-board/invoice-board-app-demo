<?php

declare(strict_types=1);

namespace App\Service\Invoice;

use App\Event\InvoiceCreatedEvent;
use App\Event\InvoicePaidEvent;
use App\Event\InvoicePublishedEvent;
use App\Exception\ZeroBalanceException;
use App\Factory\InvoiceStatusFactory;
use App\Interfaces\InvoiceProducerInterface;
use App\Model\Collection\InvoiceCollection;
use App\Model\Document\Account;
use App\Model\Document\AccountTransaction;
use App\Model\Document\Invoice;
use App\Model\Document\InvoiceBadge;
use App\Model\Document\InvoiceStatus;
use App\Model\Document\InvoiceTransaction;
use App\Model\Repository\InvoiceRepository;
use App\Service\AccountTransaction\AccountTransactionService;
use App\Service\InvoiceBadge\InvoiceBadgeService;
use App\Service\InvoiceTransaction\InvoiceTransactionService;
use App\Service\Task\Filter\TaskZeroTotalFilter;
use Exception;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class InvoiceService
{
    private const ACCOUNT_TRANSACTION_DESCRIPTION = 'Pay of invoice %s from %s.';

    private $invoiceRepository;
    private $accountTransactionService;
    private $invoiceTransactionService;
    private $invoiceSummaryService;
    private $eventDispatcher;
    private $invoiceBadgeService;
    private $invoiceStatusFactory;
    private $invoiceProducers;
    private $taskFilter;

    public function __construct(
        InvoiceRepository $invoiceRepository,
        AccountTransactionService $accountTransactionService,
        InvoiceTransactionService $invoiceTransactionService,
        InvoiceSummaryService $invoiceSummaryService,
        EventDispatcherInterface $eventDispatcher,
        InvoiceBadgeService $invoiceBadgeService,
        InvoiceStatusFactory $invoiceStatusFactory,
        TaskZeroTotalFilter $taskFilter,
        InvoiceProducerInterface ...$invoiceProducers
    ) {
        $this->invoiceRepository = $invoiceRepository;
        $this->accountTransactionService = $accountTransactionService;
        $this->invoiceTransactionService = $invoiceTransactionService;
        $this->invoiceSummaryService = $invoiceSummaryService;
        $this->eventDispatcher = $eventDispatcher;
        $this->invoiceBadgeService = $invoiceBadgeService;
        $this->invoiceStatusFactory = $invoiceStatusFactory;
        $this->invoiceProducers = $invoiceProducers;
        $this->taskFilter = $taskFilter;
    }

    public function makeDrafts(Account $account): InvoiceCollection
    {
        $invoices = new InvoiceCollection();

        foreach ($this->invoiceProducers as $producer) {
            $invoices->merge(
                $producer->produce($account)
            );
        }

        return $invoices;
    }

    public function payAll(Account $account): InvoiceCollection
    {
        $invoices = $this->invoiceRepository->findAllAccountByStatus(
            $account,
            InvoiceStatus::NAME_PENDING_PAYMENT
        );

        $payedInvoices = new InvoiceCollection();

        foreach ($invoices as $invoice) {
            if ($this->pay($invoice)) {
                $payedInvoices->add($invoice);
            }
        }

        return $payedInvoices;
    }

    private function pay(Invoice $invoice): bool
    {
        $dm = $this->invoiceRepository->getDocumentManager();
        $account = $invoice->getAccount();

        $leftToPay = $this->leftForPay($invoice);

        $accountTransaction = $this->accountTransactionService->create(
            AccountTransaction::TYPE_CREDIT,
            $leftToPay,
            sprintf(
                self::ACCOUNT_TRANSACTION_DESCRIPTION,
                $invoice->getName(),
                $invoice->getCreatedAt()->format('d/m/Y')
            )
        );

        try {
            $accountTransaction = $this->accountTransactionService->apply($account, $accountTransaction);
        } catch (ZeroBalanceException $exception) {
            $badge = $this->markAsUnpaid($invoice);

            $dm->persist($invoice);
            $dm->flush();

            $this->invoiceBadgeService->dispatchCreatedEvent($invoice, $badge);

            return false;
        }

        $invoiceTransaction = $this->invoiceTransactionService->create($accountTransaction);
        $this->invoiceTransactionService->apply($invoice, $invoiceTransaction);

        $badge = null;
        if ($invoice->getTotal() === $invoiceTransaction->getValue()) {
            $this->setStatus($invoice, InvoiceStatus::NAME_PAID);
        } else {
            $badge = $this->markAsUnpaid($invoice);
        }

        $dm->persist($accountTransaction);
        $dm->persist($account);
        $dm->persist($invoice);
        $dm->flush();

        if (null !== $badge) {
            $this->invoiceBadgeService->dispatchCreatedEvent($invoice, $badge);
        }

        if ($invoice->getStatus()->isPaid()) {
            $this->dispatchPaidEvent($invoice);
        }

        return true;
    }

    public function setStatus(Invoice $invoice, string $statusName): void
    {
        $invoiceStatus = $this->invoiceStatusFactory->create($statusName);
        $invoice->setStatus($invoiceStatus);
    }

    public function leftForPay(Invoice $invoice): int
    {
        $leftForPay = $invoice->getTotal();

        /** @var InvoiceTransaction $transaction */
        foreach ($invoice->getTransactions() as $transaction) {
            $leftForPay -= $transaction->getValue();
        }

        return $leftForPay;
    }

    public function savePreparation(Invoice $invoice): void
    {
        $filteredTasks = $this->taskFilter->filter($invoice->getTasks());
        $invoice->setTasks($filteredTasks);

        $invoiceTotal = $this->invoiceSummaryService->calculateTotal($invoice);
        $invoice->setTotal($invoiceTotal);

        $invoiceOriginalTotal = $this->invoiceSummaryService->calculateOriginalTotal($invoice);
        $invoice->setOriginalTotal($invoiceOriginalTotal);

        $invoice->setDifferenceOfTotals($invoiceTotal - $invoiceOriginalTotal);
    }

    public function save(Invoice $invoice): bool
    {
        $this->savePreparation($invoice);

        return $this->invoiceRepository->save($invoice);
    }

    public function dispatchCreatedEvent($invoice): void
    {
        $this->eventDispatcher->dispatch(
            InvoiceCreatedEvent::NAME,
            new InvoiceCreatedEvent($invoice)
        );
    }

    public function dispatchPublishEvent(Invoice $invoice): void
    {
        $this->eventDispatcher->dispatch(
            InvoicePublishedEvent::NAME,
            new InvoicePublishedEvent($invoice)
        );
    }

    public function dispatchPaidEvent(Invoice $invoice): void
    {
        $this->eventDispatcher->dispatch(
            InvoicePaidEvent::NAME,
            new InvoicePaidEvent($invoice)
        );
    }

    private function markAsUnpaid(Invoice $invoice): InvoiceBadge
    {
        $badge = $this->invoiceBadgeService->create(
            InvoiceBadge::PAYMENT_UNSUCCESSFUL_PAYMENT
        );
        $this->invoiceBadgeService->append($invoice, $badge);

        return $badge;
    }

    public function markAsPublished(Invoice $invoice): void
    {
        $this->setStatus($invoice, InvoiceStatus::NAME_PENDING_PAYMENT);
    }

    public function remove(string $id): bool
    {
        try {
            /** @var Invoice $invoice */
            $invoice = $this->invoiceRepository->find($id);

            if (null !== $invoice) {
                $this->invoiceRepository->remove($invoice);
            }

            return true;
        } catch (Exception $exception) {
            // do nothing
            return false;
        }
    }

    public function calculateSummary(Account $account, string $status): float
    {
        $invoices = $this->invoiceRepository->findAllAccountByStatus(
            $account,
            $status
        );

        $total = 0.0;

        /** @var Invoice $invoice */
        foreach ($invoices as $invoice) {
            $total += $invoice->getTotal();
        }

        return $total;
    }
}
