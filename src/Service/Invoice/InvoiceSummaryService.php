<?php

declare(strict_types=1);

namespace App\Service\Invoice;

use App\Model\Document\Invoice;
use App\Model\Document\Task;
use App\Service\Task\TaskSummaryService;

class InvoiceSummaryService
{
    private $taskSummaryService;

    public function __construct(TaskSummaryService $taskSummaryService)
    {
        $this->taskSummaryService = $taskSummaryService;
    }

    public function calculateTotal(Invoice $invoice): int
    {
        $total = 0;

        /** @var Task $task */
        foreach ($invoice->getTasks() as $task) {
            $taskTotal = $this->taskSummaryService->calculateTotal($task);
            $task->setTotal($taskTotal);

            $total += $taskTotal;
        }

        return $total;
    }

    public function calculateOriginalTotal(Invoice $invoice): int
    {
        $total = 0;

        /** @var Task $task */
        foreach ($invoice->getTasks() as $task) {
            $taskTotal = $this->taskSummaryService->calculateOriginalTotal($task);

            $total += $taskTotal;
        }

        return $total;
    }
}
