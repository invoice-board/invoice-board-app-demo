<?php

declare(strict_types=1);

namespace App\Service\Harvest;

use Psr\SimpleCache\CacheInterface;

class HarvestTimeEntryCache
{
    private const CACHE_KEY_PREFIX = 'harvest_time_entries_';
    private const CACHE_TTL = 60 * 5;

    private $cache;

    public function __construct(CacheInterface $cache)
    {
        $this->cache = $cache;
    }

    public function get(string $accountId): ?array
    {
        /* @noinspection PhpUnhandledExceptionInspection */
        return $this->cache->get(
            $this->buildCacheKey($accountId)
        );
    }

    public function save(string $accountId, array $timeEntries): void
    {
        /* @noinspection PhpUnhandledExceptionInspection */
        $this->cache->set(
            $this->buildCacheKey($accountId),
            $timeEntries,
            self::CACHE_TTL
        );
    }

    private function buildCacheKey(string $accountId): string
    {
        return self::CACHE_KEY_PREFIX . $accountId;
    }
}
