<?php

declare(strict_types=1);

namespace App\Service\Harvest\Repository;

use App\Exception\NotFoundException;
use App\Factory\TimeEntryFactory;
use App\Model\Collection\TimeEntryCollection;
use App\Model\Document\TimeEntry;
use App\Model\Repository\DeveloperRepository;
use App\Service\Harvest\Loader\AbstractHarvestTimeEntryLoader;
use App\Service\TimeEntry\TimeEntryService;

class HarvestTimeEntryRepository
{
    private $timeEntryLoader;
    private $developerRepository;
    private $timeEntryFactory;
    private $timeEntryService;

    public function __construct(
        AbstractHarvestTimeEntryLoader $timeEntryLoader,
        DeveloperRepository $developerRepository,
        TimeEntryFactory $timeEntryFactory,
        TimeEntryService $timeEntryService
    ) {
        $this->timeEntryLoader = $timeEntryLoader;
        $this->developerRepository = $developerRepository;
        $this->timeEntryFactory = $timeEntryFactory;
        $this->timeEntryService = $timeEntryService;
    }

    public function findAll(string $taskExternalId, string $accountId, string $apiToken): TimeEntryCollection
    {
        $timeEntries = new TimeEntryCollection();

        $harvestTimeEntries = $this->timeEntryLoader->load(
            $accountId,
            $apiToken
        );

        $harvestTimeEntriesFiltered = array_filter($harvestTimeEntries, static function ($item) use ($taskExternalId) {
            $harvestExternalId = (string) ($item['external_reference']['id'] ?? '');

            return $harvestExternalId === $taskExternalId;
        });

        foreach ($harvestTimeEntriesFiltered as $harvestTimeEntry) {
            $remoteUserName = (string) ($harvestTimeEntry['user']['name'] ?? '');
            $developer = $this->developerRepository->findByName($remoteUserName);
            if (null === $developer) {
                throw new NotFoundException('Developer Not Found: ' . $remoteUserName);
            }

            $timeEntry = $this->timeEntryFactory->create(
                TimeEntry::SOURCE_HARVEST,
                (float) $harvestTimeEntry['hours'],
                $developer->getName(),
                $developer->getHourlyRate()
            );

            $this->timeEntryService->smartMerge($timeEntries, $timeEntry);
        }

        return $timeEntries;
    }
}
