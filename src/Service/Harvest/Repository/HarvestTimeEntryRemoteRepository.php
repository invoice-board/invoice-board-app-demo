<?php

declare(strict_types=1);

namespace App\Service\Harvest\Repository;

use App\Exception\RuntimeException;

class HarvestTimeEntryRemoteRepository
{
    private const API_FIRST_PAGE_URL = 'https://api.harvestapp.com/v2/time_entries';

    public function findAll(string $accountId, $apiToken): array
    {
        return $this->findByPage($accountId, $apiToken, self::API_FIRST_PAGE_URL);
    }

    private function findByPage(string $accountId, string $apiToken, string $pageUrl): array
    {
        $headers = [
            'Authorization: Bearer ' . $apiToken,
            'Harvest-Account-ID: ' . $accountId,
        ];

        $handler = curl_init();
        curl_setopt($handler, CURLOPT_URL, $pageUrl);
        curl_setopt($handler, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($handler, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($handler, CURLOPT_USERAGENT, 'Invoice Board Api Client');
        $response = curl_exec($handler);
        if (curl_errno($handler)) {
            throw new RuntimeException(curl_error($handler));
        }

        $responseData = json_decode($response, true);
        $responseCode = (int) curl_getinfo($handler, CURLINFO_RESPONSE_CODE);
        curl_close($handler);

        if ($responseCode < 200 || $responseCode >= 400) {
            throw new RuntimeException($responseData['message']);
        }

        $timeEntries = $responseData['time_entries'] ?? [];
        $nextPageUrl = (string) ($responseData['links']['next'] ?? '');

        if (false === empty($nextPageUrl)) {
            $timeEntries = array_merge(
                $timeEntries,
                $this->findByPage($accountId, $apiToken, $nextPageUrl)
            );
        }

        return $timeEntries;
    }
}
