<?php

declare(strict_types=1);

namespace App\Service\Harvest\Loader;

use App\Service\Harvest\HarvestTimeEntryCache;
use App\Service\Harvest\Repository\HarvestTimeEntryRemoteRepository;

class HarvestTimeEntryRemoteLoader extends AbstractHarvestTimeEntryLoader
{
    private $cacheManager;
    private $remoteRepository;

    public function __construct(
        HarvestTimeEntryRemoteRepository $remoteRepository,
        HarvestTimeEntryCache $cacheManager,
        AbstractHarvestTimeEntryLoader $nextHandler = null
    ) {
        parent::__construct($nextHandler);

        $this->cacheManager = $cacheManager;
        $this->remoteRepository = $remoteRepository;
    }

    protected function processing(string $accountId, string $apiToken): ?array
    {
        $timeEntries = $this->remoteRepository->findAll($accountId, $apiToken);

        if (false === empty($timeEntries)) {
            $this->cacheManager->save($accountId, $timeEntries);

            return $timeEntries;
        }

        return null;
    }
}
