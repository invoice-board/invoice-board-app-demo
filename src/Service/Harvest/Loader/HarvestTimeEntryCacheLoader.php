<?php

declare(strict_types=1);

namespace App\Service\Harvest\Loader;

use App\Service\Harvest\HarvestTimeEntryCache;

class HarvestTimeEntryCacheLoader extends AbstractHarvestTimeEntryLoader
{
    private $timeEntryCacheManager;

    public function __construct(HarvestTimeEntryCache $timeEntryCacheManager, AbstractHarvestTimeEntryLoader $nextHandler = null)
    {
        parent::__construct($nextHandler);

        $this->timeEntryCacheManager = $timeEntryCacheManager;
    }

    protected function processing(string $accountId, string $apiToken): ?array
    {
        return $this->timeEntryCacheManager->get($accountId);
    }
}
