<?php

declare(strict_types=1);

namespace App\Service\Harvest\Loader;

abstract class AbstractHarvestTimeEntryLoader
{
    private $nextHandler;

    public function __construct(self $nextHandler = null)
    {
        $this->nextHandler = $nextHandler;
    }

    final public function load(string $accountId, string $apiToken): ?array
    {
        $result = $this->processing($accountId, $apiToken);

        if (null === $result && null !== $this->nextHandler) {
            $result = $this->nextHandler->load($accountId, $apiToken);
        }

        return $result;
    }

    abstract protected function processing(string $accountId, string $apiToken): ?array;
}
