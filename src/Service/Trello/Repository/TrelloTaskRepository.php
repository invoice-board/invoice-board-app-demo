<?php

declare(strict_types=1);

namespace App\Service\Trello\Repository;

use App\Factory\TaskFactory;
use App\Model\Collection\TaskCollection;
use Trello\Client;

class TrelloTaskRepository
{
    private $taskFactory;

    public function __construct(
        TaskFactory $taskFactory
    ) {
        $this->taskFactory = $taskFactory;
    }

    public function findAll(string $invoiceExternalId, Client $client): TaskCollection
    {
        $tasks = new TaskCollection();

        $cardsData = $client->lists()->cards()->all(
            $invoiceExternalId
        );

        foreach ($cardsData as $cardData) {
            $task = $this->taskFactory->create(
                (string) $cardData['name'],
                (string) $cardData['shortLink'],
                (string) $cardData['idShort'],
                (string) $cardData['name'],
                (string) $cardData['shortUrl']
            );

            $tasks->add($task);
        }

        return $tasks;
    }
}
