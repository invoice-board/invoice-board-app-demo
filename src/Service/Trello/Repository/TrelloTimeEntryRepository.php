<?php

declare(strict_types=1);

namespace App\Service\Trello\Repository;

use App\Exception\NotFoundException;
use App\Factory\TimeEntryFactory;
use App\Model\Collection\TimeEntryCollection;
use App\Model\Document\TimeEntry;
use App\Model\Repository\DeveloperRepository;
use App\Service\TimeEntry\TimeEntryService;
use App\Service\Trello\TrelloTimeParser;
use Trello\Client;

class TrelloTimeEntryRepository
{
    private $timeEntryService;
    private $developerRepository;
    private $timeEntryFactory;
    private $timeParser;

    public function __construct(
        TimeEntryService $timeEntryService,
        DeveloperRepository $developerRepository,
        TimeEntryFactory $timeEntryFactory,
        TrelloTimeParser $timeParser
    ) {
        $this->timeEntryService = $timeEntryService;
        $this->developerRepository = $developerRepository;
        $this->timeEntryFactory = $timeEntryFactory;
        $this->timeParser = $timeParser;
    }

    public function findAll(string $taskExternalId, Client $client): TimeEntryCollection
    {
        $timeEntries = new TimeEntryCollection();

        $trelloActions = $client->cards()->actions()->all($taskExternalId);

        $trelloActionsFiltered = array_filter($trelloActions, static function ($item) {
            return 'commentCard' === $item['type'] && false === empty($item['data']['text']);
        });

        foreach ($trelloActionsFiltered as $trelloAction) {
            $trackedHours = $this->timeParser->parse($trelloAction['data']['text']);

            if (null === $trackedHours) {
                continue;
            }

            $memberName = $trelloAction['memberCreator']['fullName'] ?? '';
            $developer = $this->developerRepository->findByName($memberName);
            if (null === $developer) {
                throw new NotFoundException('Developer Not Found: ' . $memberName);
            }

            $timeEntry = $this->timeEntryFactory->create(
                TimeEntry::SOURCE_TRELLO,
                $trackedHours,
                $developer->getName(),
                $developer->getHourlyRate()
            );

            $this->timeEntryService->smartMerge($timeEntries, $timeEntry);
        }

        return $timeEntries;
    }
}
