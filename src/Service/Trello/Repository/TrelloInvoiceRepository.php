<?php

declare(strict_types=1);

namespace App\Service\Trello\Repository;

use App\Factory\InvoiceFactory;
use App\Model\Collection\InvoiceCollection;
use App\Model\Document\Account;
use App\Service\Trello\TrelloClientFactory;
use Trello\Client;

class TrelloInvoiceRepository
{
    private const CARD_INVOICE_LABEL = 'Invoice:';

    private $clientFactory;
    private $invoiceFactory;

    public function __construct(TrelloClientFactory $clientFactory, InvoiceFactory $invoiceFactory)
    {
        $this->clientFactory = $clientFactory;
        $this->invoiceFactory = $invoiceFactory;
    }

    public function findAll(string $boardId, Client $client): InvoiceCollection
    {
        $invoices = new InvoiceCollection();

        $listsData = $client->board()->lists()->all(
            $boardId,
            ['filter' => 'open']
        );

        $listsDataFiltered = array_filter($listsData, static function ($item) {
            return false !== mb_strpos($item['name'], self::CARD_INVOICE_LABEL);
        });

        foreach ($listsDataFiltered as $listData) {
            $invoice = $this->invoiceFactory->create(
                trim(str_replace(self::CARD_INVOICE_LABEL, '', $listData['name'])),
                $listData['id']
            );

            $invoices->add($invoice);
        }

        return $invoices;
    }

    public function hide(Account $account, string $listId): bool
    {
        $client = $this->clientFactory->create($account);

        $result = $client->list()->setClosed($listId, true);

        return $result['closed'];
    }
}
