<?php

declare(strict_types=1);

namespace App\Service\Trello\Producer;

use App\Model\Collection\TaskCollection;
use App\Model\Document\Account;
use App\Model\Document\Invoice;
use App\Service\Trello\Repository\TrelloTaskRepository;
use App\Service\Trello\TrelloClientFactory;

class TrelloTaskProducer
{
    private $taskRepository;
    private $timeEntryProducer;
    private $clientFactory;

    public function __construct(
        TrelloTaskRepository $taskRepository,
        TrelloTimeEntryProducer $timeEntryProducer,
        TrelloClientFactory $clientFactory
    ) {
        $this->taskRepository = $taskRepository;
        $this->timeEntryProducer = $timeEntryProducer;
        $this->clientFactory = $clientFactory;
    }

    public function produce(Invoice $invoice, Account $account): TaskCollection
    {
        $client = $this->clientFactory->create($account);

        $tasks = $this->taskRepository->findAll($invoice->getExternalId(), $client);

        foreach ($tasks as $task) {
            $timeEntries = $this->timeEntryProducer->produce($task, $invoice, $account);
            $task->setTimeEntries($timeEntries);
        }

        return $tasks;
    }
}
