<?php

declare(strict_types=1);

namespace App\Service\Trello\Producer;

use App\Interfaces\InvoiceProducerInterface;
use App\Model\Collection\InvoiceCollection;
use App\Model\Document\Account;
use App\Model\Document\Invoice;
use App\Service\Trello\Repository\TrelloInvoiceRepository;
use App\Service\Trello\TrelloClientFactory;

class TrelloInvoiceProducer implements InvoiceProducerInterface
{
    private $trelloInvoiceRepository;
    private $taskProducer;
    private $clientFactory;

    public function __construct(
        TrelloInvoiceRepository $trelloInvoiceRepository,
        TrelloTaskProducer $taskProducer,
        TrelloClientFactory $clientFactory
    ) {
        $this->trelloInvoiceRepository = $trelloInvoiceRepository;
        $this->taskProducer = $taskProducer;
        $this->clientFactory = $clientFactory;
    }

    public function produce(Account $account): InvoiceCollection
    {
        $client = $this->clientFactory->create($account);

        $invoices = $this->trelloInvoiceRepository->findAll(
            $account->getTrelloSettings()->getBoardId(),
            $client
        );

        /** @var Invoice $invoice */
        foreach ($invoices as $invoice) {
            $invoice->setAccount($account);

            $tasks = $this->taskProducer->produce($invoice, $account);
            $invoice->setTasks($tasks);
        }

        return $invoices;
    }
}
