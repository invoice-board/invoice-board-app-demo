<?php

declare(strict_types=1);

namespace App\Service\Trello\Producer;

use App\Model\Collection\TimeEntryCollection;
use App\Model\Document\Account;
use App\Model\Document\Invoice;
use App\Model\Document\Task;
use App\Service\Harvest\Repository\HarvestTimeEntryRepository;
use App\Service\Trello\Repository\TrelloTimeEntryRepository;
use App\Service\Trello\TrelloClientFactory;

class TrelloTimeEntryProducer
{
    private $trelloTimeEntryRepository;
    private $harvestTimeEntryRepository;
    private $trelloClientFactory;

    public function __construct(
        TrelloTimeEntryRepository $trelloTimeEntryRepository,
        HarvestTimeEntryRepository $harvestTimeEntryRepository,
        TrelloClientFactory $trelloClientFactory
    ) {
        $this->trelloTimeEntryRepository = $trelloTimeEntryRepository;
        $this->harvestTimeEntryRepository = $harvestTimeEntryRepository;
        $this->trelloClientFactory = $trelloClientFactory;
    }

    public function produce(Task $task, Invoice $invoice, Account $account): TimeEntryCollection
    {
        $timeEntries = new TimeEntryCollection();

        $trelloClient = $this->trelloClientFactory->create($account);
        $timeEntries->merge(
            $this->trelloTimeEntryRepository->findAll($task->getExternalId(), $trelloClient)
        );

        $harvestSettings = $invoice->getAccount()->getHarvestSettings();
        $timeEntries->merge(
            $this->harvestTimeEntryRepository->findAll(
                $task->getExternalId(),
                $harvestSettings->getAccountId(),
                $harvestSettings->getApiToken()
            )
        );

        return $timeEntries;
    }
}
