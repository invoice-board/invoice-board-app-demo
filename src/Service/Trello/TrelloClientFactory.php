<?php

declare(strict_types=1);

namespace App\Service\Trello;

use App\Model\Document\Account;
use Trello\Client;

class TrelloClientFactory
{
    public function create(Account $account): Client
    {
        $trelloSettings = $account->getTrelloSettings();

        $client = new Client();
        $client->authenticate(
            $trelloSettings->getApiKey(),
            $trelloSettings->getApiSecret(),
            Client::AUTH_URL_CLIENT_ID
        );

        return $client;
    }
}
