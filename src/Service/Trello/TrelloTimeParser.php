<?php

declare(strict_types=1);

namespace App\Service\Trello;

use App\Helper\TimeConverter;

class TrelloTimeParser
{
    /**
     * @time 1h
     * @time 45m
     * @time 1h 45m
     */
    private const REGEXP = '/\@time(\s+(\d+)h)?(\s+(\d+)m)?/';

    private $timeConverter;

    public function __construct(TimeConverter $timeConverter)
    {
        $this->timeConverter = $timeConverter;
    }

    public function parse(string $text): ?float
    {
        if (preg_match(self::REGEXP, $text, $matches)) {
            $hours = (int) ($matches[2] ?? 0);
            $minutes = (int) ($matches[4] ?? 0);

            return round($this->timeConverter->toDecimal($hours, $minutes), 2);
        }

        return null;
    }
}
