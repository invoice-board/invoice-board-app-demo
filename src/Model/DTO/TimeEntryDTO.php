<?php

declare(strict_types=1);

namespace App\Model\DTO;

class TimeEntryDTO
{
    private $sourceName = '';

    private $trackedHours = 0.0;

    private $developerName = '';

    private $hourlyRate = 0;

    public function getSourceName(): string
    {
        return $this->sourceName;
    }

    public function setSourceName(string $sourceName): void
    {
        $this->sourceName = $sourceName;
    }

    public function getTrackedHours(): float
    {
        return $this->trackedHours;
    }

    public function setTrackedHours(float $trackedHours): void
    {
        $this->trackedHours = $trackedHours;
    }

    public function getDeveloperName(): string
    {
        return $this->developerName;
    }

    public function setDeveloperName(string $developerName): void
    {
        $this->developerName = $developerName;
    }

    public function getHourlyRate(): int
    {
        return $this->hourlyRate;
    }

    public function setHourlyRate(int $hourlyRate): void
    {
        $this->hourlyRate = $hourlyRate;
    }
}
