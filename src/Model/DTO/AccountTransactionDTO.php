<?php

declare(strict_types=1);

namespace App\Model\DTO;

use Symfony\Component\Validator\Constraints as Assert;

class AccountTransactionDTO
{
    /**
     * @Assert\NotBlank()
     */
    private $description = '';

    /**
     * @Assert\NotBlank()
     * @Assert\Type("float")
     * @Assert\Range(min = 0)
     */
    private $value = 0.0;

    private $type;

    public function __construct(string $type)
    {
        $this->type = $type;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getValue(): float
    {
        return $this->value;
    }

    public function setValue(float $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
