<?php

declare(strict_types=1);

namespace App\Model\Repository;

use App\Helper\SortingConstant;
use App\Model\Collection\InvoiceCollection;
use App\Model\Document\Account;
use App\Model\Document\Invoice;
use Doctrine\ODM\MongoDB\DocumentRepository;

class InvoiceRepository extends DocumentRepository
{
    public function findAllAccount(Account $account): InvoiceCollection
    {
        $result = $this->createQueryBuilder()
            ->field('account')->references($account)
            ->sort('createdAt', 'desc')
            ->getQuery()->toArray();

        return new InvoiceCollection($result);
    }

    public function findAllAccountByStatus(
        Account $account,
        string $status,
        string $setAt = SortingConstant::DESC
    ): InvoiceCollection {
        $invoices = $this->createQueryBuilder()
            ->field('account')->references($account)
            ->field('status.name')->equals($status)
            ->sort('status.setAt', $setAt)
            ->getQuery()
            ->toArray();

        return new InvoiceCollection($invoices);
    }

    public function save(Invoice $invoice): bool
    {
        $dm = $this->getDocumentManager();

        $dm->persist($invoice);

        $dm->flush();

        return true;
    }

    public function remove(Invoice $invoice): bool
    {
        $dm = $this->getDocumentManager();

        $dm->remove($invoice);

        $dm->flush();

        return true;
    }
}
