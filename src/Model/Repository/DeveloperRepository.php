<?php

declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Document\Developer;
use Doctrine\ODM\MongoDB\DocumentRepository;

class DeveloperRepository extends DocumentRepository
{
    public function findByName(string $name): ?Developer
    {
        // TODO: implement it
        $developer = new Developer();
        $developer->setName($name);
        $developer->setHourlyRate(16);

        return $developer;
    }
}
