<?php

declare(strict_types=1);

namespace App\Model\Repository;

use App\Model\Collection\AccountTransactionCollection;
use App\Model\Document\Account;
use Doctrine\ODM\MongoDB\DocumentRepository;

class AccountTransactionRepository extends DocumentRepository
{
    public function findAllByAccount(Account $account): AccountTransactionCollection
    {
        /** @noinspection PhpUnhandledExceptionInspection */
        $transactions = $this->createQueryBuilder()
            ->field('account')->references($account)
            ->sort('createdAt', 'desc')
            ->getQuery()
            ->toArray();

        return new AccountTransactionCollection($transactions);
    }
}
