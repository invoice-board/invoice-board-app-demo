<?php

declare(strict_types=1);

namespace App\Model\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument()
 */
class TimeEntry extends AbstractDocument
{
    public const SOURCE_HARVEST = 'Harvest';
    public const SOURCE_TRELLO = 'Trello';

    public const SOURCES = [
        self::SOURCE_HARVEST,
        self::SOURCE_TRELLO,
    ];

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $sourceName = '';

    /**
     * @var int
     * @ODM\Field(type="integer")
     */
    private $hourlyRate = 0;

    /**
     * @var int
     * @ODM\Field(type="integer")
     */
    private $estimatedHours = 0;

    /**
     * @var float
     * @ODM\Field(type="float")
     */
    private $trackedHours = 0.0;

    /**
     * @var float
     * @ODM\Field(type="float")
     */
    private $originalTrackedHours = 0.0;

    /**
     * @var int
     * @ODM\Field(type="integer")
     */
    private $total = 0;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $developerName = '';

    public function getSourceName(): string
    {
        return $this->sourceName;
    }

    public function setSourceName(string $sourceName): void
    {
        $this->sourceName = $sourceName;
    }

    public function getHourlyRate(): int
    {
        return $this->hourlyRate;
    }

    public function setHourlyRate(int $hourlyRate): void
    {
        $this->hourlyRate = $hourlyRate;
    }

    public function getEstimatedHours(): int
    {
        return $this->estimatedHours;
    }

    public function setEstimatedHours(int $estimatedHours): void
    {
        $this->estimatedHours = $estimatedHours;
    }

    public function getTrackedHours(): float
    {
        return $this->trackedHours;
    }

    public function setTrackedHours(float $trackedHours): void
    {
        $this->trackedHours = $trackedHours;
    }

    public function getOriginalTrackedHours(): float
    {
        return $this->originalTrackedHours;
    }

    public function setOriginalTrackedHours(float $originalTrackedHours): void
    {
        $this->originalTrackedHours = $originalTrackedHours;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    public function getDeveloperName(): string
    {
        return $this->developerName;
    }

    public function setDeveloperName(string $developerName): void
    {
        $this->developerName = $developerName;
    }
}
