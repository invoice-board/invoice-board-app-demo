<?php

declare(strict_types=1);

namespace App\Model\Document;

use App\Model\Collection\TimeEntryCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument()
 */
class Task extends AbstractDocument
{
    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $name = '';

    /**
     * @var int
     * @ODM\Field(type="integer")
     */
    private $total = 0;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $externalId = '';

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $externalNumber = '';

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $externalName = '';

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $externalUrl = '';

    /**
     * @var TimeEntryCollection
     * @ODM\EmbedMany(
     *     collectionClass="\App\Model\Collection\TimeEntryCollection",
     *     targetDocument="TimeEntry"
     * )
     */
    private $timeEntries;

    public function __construct()
    {
        $this->timeEntries = new TimeEntryCollection();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    public function getExternalId(): string
    {
        return $this->externalId;
    }

    public function setExternalId(string $externalId): void
    {
        $this->externalId = $externalId;
    }

    public function getExternalNumber(): string
    {
        return $this->externalNumber;
    }

    public function setExternalNumber(string $externalNumber): void
    {
        $this->externalNumber = $externalNumber;
    }

    public function getExternalName(): string
    {
        return $this->externalName;
    }

    public function setExternalName(string $externalName): void
    {
        $this->externalName = $externalName;
    }

    public function getExternalUrl(): string
    {
        return $this->externalUrl;
    }

    public function setExternalUrl(string $externalUrl): void
    {
        $this->externalUrl = $externalUrl;
    }

    public function getTimeEntries(): TimeEntryCollection
    {
        return $this->timeEntries;
    }

    public function setTimeEntries(TimeEntryCollection $timeEntries): void
    {
        $this->timeEntries = $timeEntries;
    }
}
