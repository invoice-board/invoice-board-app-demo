<?php

declare(strict_types=1);

namespace App\Model\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document(repositoryClass="\App\Model\Repository\DeveloperRepository")
 */
class Developer extends AbstractDocument
{
    /**
     * @var Account
     * @ODM\ReferenceOne(targetDocument="Account")
     */
    private $account;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $name = '';

    /**
     * @var int
     * @ODM\Field(type="integer")
     */
    private $hourlyRate = 0;

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function setAccount(Account $account): void
    {
        $this->account = $account;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getHourlyRate(): int
    {
        return $this->hourlyRate;
    }

    public function setHourlyRate(int $hourlyRate): void
    {
        $this->hourlyRate = $hourlyRate;
    }
}
