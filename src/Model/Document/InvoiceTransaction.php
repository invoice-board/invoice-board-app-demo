<?php

declare(strict_types=1);

namespace App\Model\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument()
 */
class InvoiceTransaction extends AbstractDocument
{
    /**
     * @var \DateTime
     * @ODM\Field(type="date")
     */
    private $createdAt;

    /**
     * @var int
     * @ODM\Field(type="integer")
     */
    private $value = 0;

    /**
     * @var AccountTransaction
     * @ODM\ReferenceOne(targetDocument="AccountTransaction")
     */
    private $accountTransaction;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function setValue(int $value): void
    {
        $this->value = $value;
    }

    public function getAccountTransaction(): AccountTransaction
    {
        return $this->accountTransaction;
    }

    public function setAccountTransaction(AccountTransaction $accountTransaction): void
    {
        $this->accountTransaction = $accountTransaction;
    }
}
