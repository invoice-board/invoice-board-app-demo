<?php

declare(strict_types=1);

namespace App\Model\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument()
 */
class InvoiceStatus extends AbstractDocument
{
    public const NAME_DRAFT = 'draft';
    public const NAME_PENDING_PAYMENT = 'pending-payment';
    public const NAME_PAID = 'paid';

    public const NAMES = [
        self::NAME_DRAFT,
        self::NAME_PENDING_PAYMENT,
        self::NAME_PAID,
    ];

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $name = self::NAME_DRAFT;

    /**
     * @var \DateTime
     * @ODM\Field(type="date")
     */
    private $setAt;

    public function __construct()
    {
        $this->setAt = new \DateTime();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getSetAt(): \DateTime
    {
        return $this->setAt;
    }

    public function setSetAt(\DateTime $setAt): void
    {
        $this->setAt = $setAt;
    }

    public function isDraft(): bool
    {
        return self::NAME_DRAFT === $this->getName();
    }

    public function isPaid(): bool
    {
        return self::NAME_PAID === $this->getName();
    }

    public function isPendingPayment(): bool
    {
        return self::NAME_PENDING_PAYMENT === $this->getName();
    }
}
