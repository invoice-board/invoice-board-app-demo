<?php

declare(strict_types=1);

namespace App\Model\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument()
 */
class TelegramSettings extends AbstractDocument
{
    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $botToken = '';

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $chatId = '';

    public function getBotToken(): string
    {
        return $this->botToken;
    }

    public function setBotToken(string $botToken): void
    {
        $this->botToken = $botToken;
    }

    public function getChatId(): string
    {
        return $this->chatId;
    }

    public function setChatId(string $chatId): void
    {
        $this->chatId = $chatId;
    }
}
