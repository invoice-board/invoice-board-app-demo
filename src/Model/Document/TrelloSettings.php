<?php

declare(strict_types=1);

namespace App\Model\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument()
 */
class TrelloSettings extends AbstractDocument
{
    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $apiKey = '';

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $apiSecret = '';

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $boardId = '';

    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    public function setApiKey(string $apiKey): self
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    public function getApiSecret(): string
    {
        return $this->apiSecret;
    }

    public function setApiSecret(string $apiSecret): void
    {
        $this->apiSecret = $apiSecret;
    }

    public function getBoardId(): string
    {
        return $this->boardId;
    }

    public function setBoardId(string $boardId): void
    {
        $this->boardId = $boardId;
    }
}
