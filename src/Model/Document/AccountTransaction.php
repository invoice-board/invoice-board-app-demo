<?php

declare(strict_types=1);

namespace App\Model\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document(repositoryClass="\App\Model\Repository\AccountTransactionRepository")
 */
class AccountTransaction extends AbstractDocument
{
    public const TYPE_CREDIT = 'credit';
    public const TYPE_DEBIT = 'debit';

    /**
     * @var Account
     * @ODM\ReferenceOne(targetDocument="Account")
     */
    private $account;

    /**
     * @var \DateTime
     * @ODM\Field(type="date")
     */
    private $createdAt;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $description = '';

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $type = self::TYPE_DEBIT;

    /**
     * @var int
     * @ODM\Field(type="integer")
     */
    private $value = 0;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function setAccount(Account $account): void
    {
        $this->account = $account;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        // TODO: check value type

        $this->type = $type;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function setValue(int $value): void
    {
        $this->value = $value;
    }
}
