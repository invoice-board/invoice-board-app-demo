<?php

declare(strict_types=1);

namespace App\Model\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument()
 */
class HarvestSettings extends AbstractDocument
{
    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $accountId = '';

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $apiToken = '';

    public function getAccountId(): string
    {
        return $this->accountId;
    }

    public function setAccountId(string $accountId): self
    {
        $this->accountId = $accountId;

        return $this;
    }

    public function getApiToken(): string
    {
        return $this->apiToken;
    }

    public function setApiToken(string $apiToken): void
    {
        $this->apiToken = $apiToken;
    }
}
