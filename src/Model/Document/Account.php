<?php

declare(strict_types=1);

namespace App\Model\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document(repositoryClass="\App\Model\Repository\AccountRepository")
 */
class Account extends AbstractDocument
{
    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $name = '';

    /**
     * @var int
     * @ODM\Field(type="integer")
     */
    private $value = 0;

    /**
     * @var TrelloSettings
     * @ODM\EmbedOne(targetDocument="TrelloSettings")
     */
    private $trelloSettings;

    /**
     * @var HarvestSettings
     * @ODM\EmbedOne(targetDocument="HarvestSettings")
     */
    private $harvestSettings;

    /**
     * @var TelegramSettings
     * @ODM\EmbedOne(targetDocument="TelegramSettings")
     */
    private $telegramSettings;

    public function __construct()
    {
        $this->trelloSettings = new TrelloSettings();
        $this->harvestSettings = new HarvestSettings();
        $this->telegramSettings = new TelegramSettings();
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function setValue(int $value): void
    {
        $this->value = $value;
    }

    public function getTrelloSettings(): TrelloSettings
    {
        return $this->trelloSettings;
    }

    public function setTrelloSettings(TrelloSettings $trelloSettings): void
    {
        $this->trelloSettings = $trelloSettings;
    }

    public function getHarvestSettings(): HarvestSettings
    {
        return $this->harvestSettings;
    }

    public function setHarvestSettings(HarvestSettings $harvestSettings): void
    {
        $this->harvestSettings = $harvestSettings;
    }

    public function getTelegramSettings(): TelegramSettings
    {
        return $this->telegramSettings;
    }

    public function setTelegramSettings(TelegramSettings $telegramSettings): void
    {
        $this->telegramSettings = $telegramSettings;
    }
}
