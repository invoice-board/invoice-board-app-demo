<?php

declare(strict_types=1);

namespace App\Model\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations\Id;
use Doctrine\ODM\MongoDB\Mapping\Annotations\MappedSuperclass;

/** @MappedSuperclass */
abstract class AbstractDocument
{
    /**
     * @var string
     * @Id()
     */
    protected $id;

    public function getId(): ?string
    {
        return $this->id;
    }
}
