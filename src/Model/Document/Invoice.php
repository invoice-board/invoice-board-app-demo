<?php

declare(strict_types=1);

namespace App\Model\Document;

use App\Model\Collection\InvoiceBadgeCollection;
use App\Model\Collection\InvoiceTransactionCollection;
use App\Model\Collection\TaskCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document(repositoryClass="\App\Model\Repository\InvoiceRepository")
 */
class Invoice extends AbstractDocument
{
    /**
     * @var Account
     * @ODM\ReferenceOne(targetDocument="Account")
     */
    private $account;

    /**
     * @var \DateTime
     * @ODM\Field(type="date")
     */
    private $createdAt;

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $externalId = '';

    /**
     * @var string
     * @ODM\Field(type="string")
     */
    private $name = '';

    /**
     * @var int
     * @ODM\Field(type="integer")
     */
    private $total = 0;

    /**
     * @var int
     * @ODM\Field(type="integer")
     */
    private $originalTotal = 0;

    /**
     * @var int
     * @ODM\Field(type="integer")
     */
    private $differenceOfTotals = 0;

    /**
     * @var InvoiceStatus
     * @ODM\EmbedOne(targetDocument="InvoiceStatus")
     */
    private $status;

    /**
     * @var TaskCollection
     * @ODM\EmbedMany(
     *     collectionClass="\App\Model\Collection\TaskCollection",
     *     targetDocument="Task"
     * )
     */
    private $tasks;

    /**
     * @var InvoiceTransactionCollection
     * @ODM\EmbedMany(
     *     collectionClass="\App\Model\Collection\InvoiceTransactionCollection",
     *     targetDocument="InvoiceTransaction"
     * )
     */
    private $transactions;

    /**
     * @var InvoiceBadgeCollection
     * @ODM\EmbedMany(
     *     collectionClass="\App\Model\Collection\InvoiceBadgeCollection",
     *     targetDocument="InvoiceBadge"
     * )
     */
    private $badges;

    public function __construct()
    {
        $this->createdAt = new \DateTime();
        $this->tasks = new TaskCollection();
        $this->transactions = new InvoiceTransactionCollection();
        $this->status = new InvoiceStatus();
        $this->badges = new InvoiceBadgeCollection();
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function setAccount(Account $account): void
    {
        $this->account = $account;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    public function getExternalId(): string
    {
        return $this->externalId;
    }

    public function setExternalId(string $externalId): void
    {
        $this->externalId = $externalId;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getStatus(): InvoiceStatus
    {
        return $this->status;
    }

    public function setStatus(InvoiceStatus $status): void
    {
        $this->status = $status;
    }

    public function getTasks(): TaskCollection
    {
        return $this->tasks;
    }

    public function setTasks(TaskCollection $tasks): void
    {
        $this->tasks = $tasks;
    }

    public function getTransactions(): InvoiceTransactionCollection
    {
        return $this->transactions;
    }

    public function getBadges(): InvoiceBadgeCollection
    {
        return $this->badges;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function setTotal(int $total): void
    {
        $this->total = $total;
    }

    public function getOriginalTotal(): int
    {
        return $this->originalTotal;
    }

    public function setOriginalTotal(int $originalTotal): void
    {
        $this->originalTotal = $originalTotal;
    }

    public function getDifferenceOfTotals(): int
    {
        return $this->differenceOfTotals;
    }

    public function setDifferenceOfTotals(int $differenceOfTotals): void
    {
        $this->differenceOfTotals = $differenceOfTotals;
    }
}
