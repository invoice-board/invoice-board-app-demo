<?php

declare(strict_types=1);

namespace App\Model\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument()
 */
class InvoiceBadge extends AbstractDocument
{
    public const GROUP_PAYMENT = 100;

    public const GROUPS = [
        self::GROUP_PAYMENT,
    ];

    public const PAYMENT_UNSUCCESSFUL_PAYMENT = 101;

    public const CODES = [
        self::PAYMENT_UNSUCCESSFUL_PAYMENT,
    ];

    /**
     * @var int
     * @ODM\Field(type="integer")
     */
    private $code = 0;

    public function getCode(): int
    {
        return $this->code;
    }

    public function setCode(int $code): void
    {
        //TODO: check code

        $this->code = $code;
    }

    public function isPaymentGroup(): bool
    {
        return $this->code >= self::GROUP_PAYMENT && $this->code < (self::GROUP_PAYMENT + 100);
    }
}
