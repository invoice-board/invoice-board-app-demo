<?php

declare(strict_types=1);

namespace App\Model\Collection;

use App\Model\Document\AbstractDocument;
use Doctrine\Common\Collections\ArrayCollection;

abstract class AbstractDocumentCollection extends ArrayCollection
{
    public function __construct(array $elements = [])
    {
        parent::__construct();

        foreach ($elements as $key => $val) {
            $this->set($key, $val);
        }
    }

    /**
     * @param $id
     *
     * @return mixed|null
     */
    protected function doFind($id)
    {
        foreach ($this->getKeys() as $key) {
            $item = $this->get($key);

            if ($item instanceof AbstractDocument && $item->getId() === $id) {
                return $item;
            }
        }

        return null;
    }

    public function first()
    {
        $result = parent::first();

        return false === $result ? null : $result;
    }

    public function last()
    {
        $result = parent::last();

        return false === $result ? null : $result;
    }

    public function current()
    {
        $result = parent::current();

        return false === $result ? null : $result;
    }

    public function merge(self $collections): self
    {
        foreach ($collections as $key => $element) {
            if (is_numeric($key)) {
                $this->add($element);
            } else {
                $this->set($key, $element);
            }
        }

        return $this;
    }

    /**
     * Be careful, this method returns NEW collection.
     *
     * @param \Closure $closure
     *
     * @return static
     */
    public function sort(\Closure $closure)
    {
        $sorted = $this->toArray();
        uasort($sorted, $closure);

        return $this->createFrom($sorted);
    }
}
