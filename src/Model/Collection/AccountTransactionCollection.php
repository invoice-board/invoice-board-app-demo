<?php

declare(strict_types=1);

namespace App\Model\Collection;

use App\Model\Document\AccountTransaction;

class AccountTransactionCollection extends AbstractDocumentCollection
{
    public function set($key, $value)
    {
        // TODO: check type

        parent::set($key, $value);
    }

    public function add($element)
    {
        // TODO: check type

        return parent::add($element);
    }

    public function find($id): ?AccountTransaction
    {
        return $this->doFind($id);
    }
}
