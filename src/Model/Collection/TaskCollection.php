<?php

declare(strict_types=1);

namespace App\Model\Collection;

use App\Model\Document\Task;

class TaskCollection extends AbstractDocumentCollection
{
    public function set($key, $value)
    {
        // TODO: check type

        parent::set($key, $value);
    }

    public function add($element)
    {
        // TODO: check type

        return parent::add($element);
    }

    public function find($id): ?Task
    {
        return $this->doFind($id);
    }
}
