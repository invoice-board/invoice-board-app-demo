<?php

declare(strict_types=1);

namespace App\Model\Collection;

use App\Model\Document\Task;
use App\Model\Document\TimeEntry;

class TimeEntryCollection extends AbstractDocumentCollection
{
    public function set($key, $value)
    {
        // TODO: check type

        parent::set($key, $value);
    }

    public function add($element)
    {
        // TODO: check type

        return parent::add($element);
    }

    public function find($id): ?Task
    {
        return $this->doFind($id);
    }

    public function findByDeveloperName(string $name): ?TimeEntry
    {
        /** @var TimeEntry $item */
        foreach ($this->getValues() as $item) {
            if ($item->getDeveloperName() === $name) {
                return $item;
            }
        }

        return null;
    }
}
