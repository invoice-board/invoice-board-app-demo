<?php

declare(strict_types=1);

namespace App\Twig;

use App\Helper\MoneyConverter;
use App\Helper\MoneyViewer;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class AppExtension extends AbstractExtension
{
    private $moneyConverter;
    private $moneyViewer;

    public function __construct(MoneyConverter $moneyConverter, MoneyViewer $moneyViewer)
    {
        $this->moneyConverter = $moneyConverter;
        $this->moneyViewer = $moneyViewer;
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('centsToDollar', [$this->moneyConverter, 'centsToDollars']),
            new TwigFilter('dollarFormat', [$this->moneyViewer, 'asDollar']),
        ];
    }
}
