<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Event\InvoicePaidEvent;
use App\Service\Notification\Message\MessageRegistry;
use App\Service\Notification\NotificationService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class InvoicePaidListener implements EventSubscriberInterface
{
    private $messageRegistry;
    private $notificationService;

    public function __construct(
        MessageRegistry $messageRegistry,
        NotificationService $notificationService
    ) {
        $this->messageRegistry = $messageRegistry;
        $this->notificationService = $notificationService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            InvoicePaidEvent::NAME => [
                ['sendNotification', 0],
            ],
        ];
    }

    public function sendNotification(InvoicePaidEvent $event): void
    {
        $invoice = $event->getInvoice();
        $account = $invoice->getAccount();

        $this->notificationService->send(
            $account,
            $this->messageRegistry->getInvoicePaid($invoice)
        );
    }
}
