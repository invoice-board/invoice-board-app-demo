<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Event\InvoiceBadgeCreatedEvent;
use App\Model\Document\InvoiceBadge;
use App\Service\Notification\Message\MessageRegistry;
use App\Service\Notification\NotificationService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class InvoiceBadgeCreatedListener implements EventSubscriberInterface
{
    private $messageRegistry;
    private $notificationService;

    public function __construct(
        MessageRegistry $messageRegistry,
        NotificationService $notificationService
    ) {
        $this->messageRegistry = $messageRegistry;
        $this->notificationService = $notificationService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            InvoiceBadgeCreatedEvent::NAME => [
                ['sendNotification', 0],
            ],
        ];
    }

    public function sendNotification(InvoiceBadgeCreatedEvent $event): void
    {
        $badge = $event->getBadge();
        $invoice = $event->getInvoice();
        $account = $invoice->getAccount();

        if (InvoiceBadge::PAYMENT_UNSUCCESSFUL_PAYMENT === $badge->getCode()) {
            $this->notificationService->send(
                $account,
                $this->messageRegistry->getNotEnoughMoneyMessage($invoice)
            );
        }
    }
}
