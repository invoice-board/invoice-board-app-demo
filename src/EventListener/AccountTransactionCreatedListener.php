<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Event\AccountTransactionCreatedEvent;
use App\Service\Account\AccountService;
use App\Service\Invoice\InvoiceService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class AccountTransactionCreatedListener implements EventSubscriberInterface
{
    private $invoiceService;
    private $accountService;

    public function __construct(InvoiceService $invoiceService, AccountService $accountService)
    {
        $this->invoiceService = $invoiceService;
        $this->accountService = $accountService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            AccountTransactionCreatedEvent::NAME => [
                ['payAllInvoices', 0],
            ],
        ];
    }

    public function payAllInvoices(AccountTransactionCreatedEvent $event): void
    {
        $account = $this->accountService->getCurrent();
        $this->invoiceService->payAll($account);
    }
}
