<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Event\InvoicePublishedEvent;
use App\Service\Account\AccountService;
use App\Service\Invoice\InvoiceService;
use App\Service\Notification\Message\MessageRegistry;
use App\Service\Notification\NotificationService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class InvoicePublishedListener implements EventSubscriberInterface
{
    private $invoiceService;
    private $notificationService;
    private $messageRegistry;
    private $accountService;

    public function __construct(
        InvoiceService $invoiceService,
        NotificationService $notificationService,
        MessageRegistry $messageRegistry,
        AccountService $accountService
    ) {
        $this->invoiceService = $invoiceService;
        $this->notificationService = $notificationService;
        $this->messageRegistry = $messageRegistry;
        $this->accountService = $accountService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            InvoicePublishedEvent::NAME => [
                ['sendNotification', 0],
                ['payAllInvoices', -1],
            ],
        ];
    }

    public function sendNotification(InvoicePublishedEvent $event): void
    {
        $invoice = $event->getInvoice();
        $account = $invoice->getAccount();

        $this->notificationService->send(
            $account,
            $this->messageRegistry->getInvoicePublishedMessage($invoice)
        );
    }

    public function payAllInvoices(InvoicePublishedEvent $event): void
    {
        $account = $this->accountService->getCurrent();
        $this->invoiceService->payAll($account);
    }
}
