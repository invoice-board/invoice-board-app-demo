<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Event\InvoiceCreatedEvent;
use App\Service\Notification\Message\MessageRegistry;
use App\Service\Notification\NotificationService;
use App\Service\Trello\Repository\TrelloInvoiceRepository;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class InvoiceCreatedListener implements EventSubscriberInterface
{
    private $trelloInvoiceRepository;
    private $messageRegistry;
    private $notificationService;

    public function __construct(
        TrelloInvoiceRepository $trelloInvoiceRepository,
        MessageRegistry $messageRegistry,
        NotificationService $notificationService
    ) {
        $this->trelloInvoiceRepository = $trelloInvoiceRepository;
        $this->messageRegistry = $messageRegistry;
        $this->notificationService = $notificationService;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            InvoiceCreatedEvent::NAME => [
                ['archiveTrelloList', 0],
                ['sendNotification', -1],
            ],
        ];
    }

    public function archiveTrelloList(InvoiceCreatedEvent $event): void
    {
        $invoice = $event->getInvoice();
        $account = $invoice->getAccount();

        $this->trelloInvoiceRepository->hide($account, $invoice->getExternalId());
    }

    public function sendNotification(InvoiceCreatedEvent $event): void
    {
        $invoice = $event->getInvoice();
        $account = $invoice->getAccount();

        $this->notificationService->send(
            $account,
            $this->messageRegistry->getInvoiceCreated($invoice)
        );
    }
}
