<?php

declare(strict_types=1);

namespace App\Factory;

use App\Model\Document\InvoiceStatus;

class InvoiceStatusFactory
{
    public function create(string $name): InvoiceStatus
    {
        $status = new InvoiceStatus();
        $status->setName($name);

        return $status;
    }
}
