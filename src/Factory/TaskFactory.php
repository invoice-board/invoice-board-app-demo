<?php

declare(strict_types=1);

namespace App\Factory;

use App\Model\Document\Task;

class TaskFactory
{
    public function create(
        string $name,
        string $externalId,
        string $externalNumber,
        string $externalName,
        string $externalUrl
    ): Task {
        $task = new Task();

        $task->setName($name);
        $task->setExternalId($externalId);
        $task->setExternalNumber($externalNumber);
        $task->setExternalName($externalName);
        $task->setExternalUrl($externalUrl);

        return $task;
    }
}
