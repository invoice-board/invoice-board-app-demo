<?php

declare(strict_types=1);

namespace App\Factory;

use App\Model\Document\TimeEntry;

class TimeEntryFactory
{
    public function create(string $sourceName, float $trackedHours, string $developerName, int $hourlyRate): TimeEntry
    {
        $timeEntry = new TimeEntry();
        $timeEntry->setSourceName($sourceName);
        $timeEntry->setTrackedHours($trackedHours);
        $timeEntry->setOriginalTrackedHours($trackedHours);
        $timeEntry->setDeveloperName($developerName);
        $timeEntry->setHourlyRate($hourlyRate);

        return $timeEntry;
    }
}
