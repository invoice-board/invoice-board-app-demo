<?php

declare(strict_types=1);

namespace App\Factory;

use App\Model\Document\Invoice;

class InvoiceFactory
{
    public function create(string $name, string $externalId): Invoice
    {
        $invoice = new Invoice();
        $invoice->setExternalId($externalId);
        $invoice->setName($name);

        return $invoice;
    }
}
