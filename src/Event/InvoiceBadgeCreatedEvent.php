<?php

declare(strict_types=1);

namespace App\Event;

use App\Model\Document\Invoice;
use App\Model\Document\InvoiceBadge;
use Symfony\Component\EventDispatcher\Event;

class InvoiceBadgeCreatedEvent extends Event
{
    public const NAME = 'invoice-badge.created';

    private $badge;
    private $invoice;

    public function __construct(Invoice $invoice, InvoiceBadge $badge)
    {
        $this->badge = $badge;
        $this->invoice = $invoice;
    }

    public function getBadge(): InvoiceBadge
    {
        return $this->badge;
    }

    public function getInvoice(): Invoice
    {
        return $this->invoice;
    }
}
