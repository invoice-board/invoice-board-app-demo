<?php

declare(strict_types=1);

namespace App\Event;

use App\Model\Document\Invoice;
use Symfony\Component\EventDispatcher\Event;

class InvoicePublishedEvent extends Event
{
    public const NAME = 'invoice.published';

    private $invoice;

    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    public function getInvoice(): Invoice
    {
        return $this->invoice;
    }
}
