<?php

declare(strict_types=1);

namespace App\Event;

use App\Model\Document\Invoice;
use Symfony\Component\EventDispatcher\Event;

class InvoiceCreatedEvent extends Event
{
    public const NAME = 'invoice.created';

    private $invoice;

    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    public function getInvoice(): Invoice
    {
        return $this->invoice;
    }
}
