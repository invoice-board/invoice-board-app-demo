<?php

declare(strict_types=1);

namespace App\Event;

use App\Model\Document\AccountTransaction;
use Symfony\Component\EventDispatcher\Event;

class AccountTransactionCreatedEvent extends Event
{
    public const NAME = 'account-transaction.created';

    private $accountTransaction;

    public function __construct(AccountTransaction $accountTransaction)
    {
        $this->accountTransaction = $accountTransaction;
    }

    public function getTransaction(): AccountTransaction
    {
        return $this->accountTransaction;
    }
}
