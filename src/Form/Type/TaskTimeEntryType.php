<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Model\Document\TimeEntry;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Type;

class TaskTimeEntryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('developerName', TextType::class, [
            'label' => false,
            'required' => true,
        ]);

        $builder->add('hourlyRate', IntegerType::class, [
            'label' => false,
            'required' => true,
            'constraints' => [
                new Type('integer'),
                new Range(['min' => 0]),
            ],
        ]);

        $builder->add('trackedHours', NumberType::class, [
            'label' => false,
            'required' => true,
            'constraints' => [
                new Type('float'),
                new Range(['min' => 0]),
            ],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TimeEntry::class,
            'csrf_protection' => false,
        ]);
    }
}
