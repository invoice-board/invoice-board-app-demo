<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Model\Document\TimeEntry;
use App\Model\DTO\TimeEntryDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Type;

class TimeEntryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('developerName', TextType::class, [
            'required' => true,
        ]);

        $builder->add('sourceName', ChoiceType::class, [
            'required' => true,
            'choices' => [
                TimeEntry::SOURCE_TRELLO => TimeEntry::SOURCE_TRELLO,
                TimeEntry::SOURCE_HARVEST => TimeEntry::SOURCE_HARVEST,
            ],
        ]);

        $builder->add('hourlyRate', IntegerType::class, [
            'required' => true,
            'constraints' => [
                new Type('integer'),
                new Range(['min' => 0]),
            ],
        ]);

        $builder->add('trackedHours', NumberType::class, [
            'required' => true,
            'constraints' => [
                new Type('float'),
                new Range(['min' => 0]),
            ],
        ]);

        $builder->add('save', SubmitType::class, [
            'label' => 'Create',
            'attr' => ['class' => 'btn btn-default pull-right'],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => TimeEntryDTO::class,
            'csrf_protection' => false,
        ]);
    }
}
