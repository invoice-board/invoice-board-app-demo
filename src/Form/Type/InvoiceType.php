<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Model\Document\Invoice;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InvoiceType extends AbstractType
{
    public const BUTTON_SAVE = 'save';
    public const BUTTON_SAVE_AND_PUBLISH = 'save_and_publish';

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('name', TextType::class, [
            'label' => 'Invoice Name',
            'required' => true,
        ]);

        $builder->add('tasks', CollectionType::class, [
            'entry_type' => InvoiceTaskType::class,
            'entry_options' => ['label' => false],
        ]);

        $builder->add(self::BUTTON_SAVE, SubmitType::class, [
            'label' => 'Save Invoice',
        ]);

        $builder->add(self::BUTTON_SAVE_AND_PUBLISH, SubmitType::class, [
            'label' => 'Save and Publish',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Invoice::class,
            'csrf_protection' => false,
        ]);
    }
}
