<?php

declare(strict_types=1);

namespace App\Form\Type;

use App\Model\DTO\AccountTransactionDTO;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AccountTransactionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('value', NumberType::class, [
            'label' => 'Value',
            'required' => true,
        ]);

        $builder->add('description', TextareaType::class, [
            'label' => 'Description',
            'required' => true,
            'empty_data' => '',
        ]);

        $builder->add('save', SubmitType::class, [
            'label' => 'Create Payment',
        ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => AccountTransactionDTO::class,
            'csrf_protection' => false,
        ]);
    }
}
