<?php

declare(strict_types=1);

namespace App\Interfaces;

use App\Model\Document\Account;

interface InvoiceProducerInterface
{
    public function produce(Account $account);
}
