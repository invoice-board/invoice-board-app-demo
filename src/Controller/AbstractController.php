<?php

declare(strict_types=1);

namespace App\Controller;

use App\Exception\ExceptionMessage;
use App\HttpException\NotFoundHttpException;
use App\Model\Document\Invoice;
use App\Model\Repository\InvoiceRepository;
use App\Service\Account\AccountService;

class AbstractController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    protected $accountService;
    protected $invoiceRepository;

    public function __construct(AccountService $accountService, InvoiceRepository $invoiceRepository)
    {
        $this->accountService = $accountService;
        $this->invoiceRepository = $invoiceRepository;
    }

    protected function findInvoice(string $invoiceId): Invoice
    {
        $account = $this->accountService->getCurrent();

        /** @var Invoice $invoice */
        /** @noinspection PhpUnhandledExceptionInspection */
        $invoice = $this->invoiceRepository->createQueryBuilder()
            ->field('account')->references($account)
            ->field('id')->equals(new \MongoId($invoiceId))
            ->getQuery()
            ->getSingleResult();

        if (null === $invoice) {
            throw new NotFoundHttpException(
                sprintf(ExceptionMessage::INVOICE_BY_ID_NOT_FOUND, $invoiceId)
            );
        }

        return $invoice;
    }
}
