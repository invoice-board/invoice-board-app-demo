<?php

declare(strict_types=1);

namespace App\Controller;

use App\Exception\ExceptionMessage;
use App\Form\Type\InvoiceType;
use App\HttpException\AccessDeniedHttpException;
use App\Model\Repository\InvoiceRepository;
use App\Service\Account\AccountService;
use App\Service\Invoice\InvoiceService;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class InvoiceController extends AbstractController
{
    private $invoiceService;

    public function __construct(
        InvoiceRepository $invoiceRepository,
        AccountService $accountService,
        InvoiceService $invoiceService
    ) {
        parent::__construct($accountService, $invoiceRepository);

        $this->invoiceRepository = $invoiceRepository;
        $this->accountService = $accountService;
        $this->invoiceService = $invoiceService;
    }

    public function listAction(): Response
    {
        $account = $this->accountService->getCurrent();
        $invoices = $this->invoiceRepository->findAllAccount($account);

        return $this->render('invoice/list.html.twig', [
            'invoices' => $invoices,
        ]);
    }

    public function viewAction(string $id): Response
    {
        $invoice = $this->findInvoice($id);
        $transactions = $invoice->getTransactions();

        return $this->render('invoice/view.html.twig', [
            'invoice' => $invoice,
            'transactions' => $transactions,
        ]);
    }

    public function printAction(string $id): Response
    {
        $invoice = $this->findInvoice($id);

        return $this->render('invoice/print.html.twig', [
            'invoice' => $invoice,
        ]);
    }

    public function updateAction(string $id, Request $request): Response
    {
        $invoice = $this->findInvoice($id);

        if (false === $invoice->getStatus()->isDraft()) {
            throw new AccessDeniedHttpException(ExceptionMessage::ACCESS_DENIED);
        }

        $form = $this->createForm(InvoiceType::class, $invoice);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var Form $form */
            $clickedButton = $form->getClickedButton();

            if ($clickedButton && InvoiceType::BUTTON_SAVE_AND_PUBLISH === $clickedButton->getName()) {
                $this->invoiceService->markAsPublished($invoice);
            }

            if ($this->invoiceService->save($invoice)) {
                if ($invoice->getStatus()->isPendingPayment()) {
                    $this->invoiceService->dispatchPublishEvent($invoice);
                }

                if (false === $invoice->getStatus()->isDraft()) {
                    return $this->redirectToRoute('invoice-view', [
                        'id' => $invoice->getId(),
                    ]);
                }
            }
        }

        return $this->render('invoice/update.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
