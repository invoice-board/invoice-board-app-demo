<?php

declare(strict_types=1);

namespace App\Controller;

use App\Form\Type\AccountTransactionType;
use App\Model\Document\AccountTransaction;
use App\Model\Document\InvoiceStatus;
use App\Model\DTO\AccountTransactionDTO;
use App\Model\Repository\AccountRepository;
use App\Model\Repository\AccountTransactionRepository;
use App\Model\Repository\InvoiceRepository;
use App\Service\Account\AccountService;
use App\Service\AccountTransaction\AccountTransactionService;
use App\Service\Invoice\InvoiceService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AccountController extends AbstractController
{
    private $accountTransactionService;
    private $accountRepository;
    private $accountTransactionRepository;
    private $invoiceService;

    public function __construct(
        AccountService $accountService,
        AccountTransactionService $accountTransactionService,
        AccountRepository $accountRepository,
        AccountTransactionRepository $accountTransactionRepository,
        InvoiceRepository $invoiceRepository,
        InvoiceService $invoiceService
    ) {
        parent::__construct($accountService, $invoiceRepository);

        $this->accountTransactionService = $accountTransactionService;
        $this->accountRepository = $accountRepository;
        $this->accountTransactionRepository = $accountTransactionRepository;
        $this->invoiceService = $invoiceService;
    }

    public function refillAction(Request $request): Response
    {
        $account = $this->accountService->getCurrent();

        $transactionDTO = new AccountTransactionDTO(
            AccountTransaction::TYPE_DEBIT
        );
        $form = $this->createForm(AccountTransactionType::class, $transactionDTO);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $transaction = $this->accountTransactionService->createFromDTO($transactionDTO);
            $transaction = $this->accountTransactionService->apply($account, $transaction);

            // todo: Move save in to repository
            $dm = $this->accountRepository->getDocumentManager();
            $dm->persist($transaction);
            $dm->persist($account);
            $dm->flush();

            $this->accountTransactionService->dispatchCreatedEvent($transaction);

            return $this->redirectToRoute('account-view');
        }

        return $this->render('account/refill.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function viewAction(): Response
    {
        $account = $this->accountService->getCurrent();

        $transactions = $this->accountTransactionRepository->findAllByAccount($account);

        $summaryPaid = $this->invoiceService->calculateSummary(
            $account,
            InvoiceStatus::NAME_PAID
        );
        $summaryPendingPayment = $this->invoiceService->calculateSummary(
            $account,
            InvoiceStatus::NAME_PENDING_PAYMENT
        );
        $summaryDraft = $this->invoiceService->calculateSummary(
            $account,
            InvoiceStatus::NAME_DRAFT
        );

        return $this->render('account/view.html.twig', [
            'account' => $account,
            'transactions' => $transactions,
            'summaryPaid' => $summaryPaid,
            'summaryPendingPayment' => $summaryPendingPayment,
            'summaryDraft' => $summaryDraft,
        ]);
    }
}
