<?php

declare(strict_types=1);

namespace App\Controller;

use App\Exception\ExceptionMessage;
use App\Factory\TimeEntryFactory;
use App\Form\Type\TimeEntryType;
use App\HttpException\AccessDeniedHttpException;
use App\HttpException\NotFoundHttpException;
use App\Model\DTO\TimeEntryDTO;
use App\Model\Repository\InvoiceRepository;
use App\Service\Account\AccountService;
use App\Service\Invoice\InvoiceSummaryService;
use App\Service\TimeEntry\TimeEntryService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TimeEntryController extends AbstractController
{
    private $timeEntryService;
    private $invoiceSummaryService;
    private $timeEntryFactory;

    public function __construct(
        AccountService $accountService,
        InvoiceRepository $invoiceRepository,
        TimeEntryService $timeEntryService,
        InvoiceSummaryService $invoiceSummaryService,
        TimeEntryFactory $timeEntryFactory
    ) {
        parent::__construct($accountService, $invoiceRepository);

        $this->timeEntryService = $timeEntryService;
        $this->invoiceSummaryService = $invoiceSummaryService;
        $this->timeEntryFactory = $timeEntryFactory;
    }

    public function createAction(string $invoiceId, string $taskId, Request $request): Response
    {
        $invoice = $this->findInvoice($invoiceId);

        if (false === $invoice->getStatus()->isDraft()) {
            throw new AccessDeniedHttpException(ExceptionMessage::ACCESS_DENIED);
        }

        $task = $invoice->getTasks()->find($taskId);

        if (null === $task) {
            throw new NotFoundHttpException(
                sprintf(ExceptionMessage::TASK_BY_ID_NOT_FOUND, $invoiceId)
            );
        }

        $timeEntryDTO = new TimeEntryDTO();
        $form = $this->createForm(TimeEntryType::class, $timeEntryDTO);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $timeEntry = $this->timeEntryFactory->create(
                $timeEntryDTO->getSourceName(),
                $timeEntryDTO->getTrackedHours(),
                $timeEntryDTO->getDeveloperName(),
                $timeEntryDTO->getHourlyRate()
            );
            $taskTimeEntries = $task->getTimeEntries();
            $this->timeEntryService->smartMerge($taskTimeEntries, $timeEntry);
            $task->setTimeEntries($taskTimeEntries);

            $invoiceTotal = $this->invoiceSummaryService->calculateTotal($invoice);
            $invoice->setTotal($invoiceTotal);

            $dm = $this->invoiceRepository->getDocumentManager();
            $dm->persist($invoice);
            $dm->flush();

            return $this->redirectToRoute('invoice-update', ['id' => $invoice->getId()]);
        }

        return $this->render('time-entry/create.html.twig', [
            'form' => $form->createView(),
            'task' => $task,
        ]);
    }
}
