version: '3.6'
services:

  nginx:
    restart: always
    image: registry.gitlab.com/invoice-board/invoice-board-nginx:1-alpine
    volumes:
      - phpsocket:/var/run
      - ./:/application:cached
    ports:
      - 8080:80
    depends_on:
      - php-fpm

  xhgui:
    image: edyan/xhgui:php7.2
    ports:
      - 8087:80

  php-fpm:
    restart: always
    image: registry.gitlab.com/invoice-board/invoice-board-php:7.2-fpm-alpine-dev
    volumes:
      - phpsocket:/var/run
      - ./:/application:cached
      # SSH key (for composer)
      - ~/.ssh/id_rsa:/root/.ssh/id_rsa
    environment:
      # XDebug
      XDEBUG_CONFIG: remote_enable=0 remote_host=host.docker.internal
      PHP_IDE_CONFIG: serverName=invoice-board-app
    # Import .env file only if APP_ENV=prod
  #    env_file:
  #    - .env

  mongo:
    restart: always
    image: mongo:latest
    command: mongod --auth
    ports:
      - 27017:27017
    volumes:
      - mongo_data:/data/db

  redis:
    restart: always
    image: redis:4-alpine
    ports:
      - 6379:6379

volumes:
  phpsocket:
    driver: local
  mongo_data:
    driver: local
