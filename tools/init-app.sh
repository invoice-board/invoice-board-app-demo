#!/bin/sh
set -eo pipefail

###
# This script will be run each time the container is started.
###

# Clear Doctrine metadata cache
php bin/console doctrine:mongodb:cache:clear-metadata --no-debug
# Clear Symfony Cache
php bin/console cache:clear --env=prod --no-debug
# Warms up Symfony Cache
php bin/console cache:warmup --env=prod --no-debug

# Make "var" directory is writable
# See http://symfony.com/doc/current/setup/file_permissions.html
chown -R www-data:www-data var
