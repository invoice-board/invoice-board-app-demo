#!/bin/sh

export PATH=$PATH:/usr/local/bin

docker-compose run --rm --no-deps php-fpm php vendor/bin/php-cs-fixer fix
